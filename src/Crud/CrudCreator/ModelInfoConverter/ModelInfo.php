<?php

namespace GreenPlate\Crud\CrudCreator\ModelInfoConverter;

class ModelInfo
{
    protected $tableName;

    protected $modifers = [];

    protected $fillables = [];

    protected $migrationPath = '';

    protected $relationships = [];

    protected $options = [];

    public function __construct($tableName, $columnInfos)
    {
        $this->initializeModelInfo($tableName, $columnInfos);
    }

    public function initializeModelInfo($tableName, $columnInfos)
    {
        $this->tableName = $tableName;

        $this->migrationPath = $columnInfos['migrationPath'];
        unset($columnInfos['migrationPath']);

        foreach ($columnInfos as $columnName => $columnInfo) {
            if (isset($columnInfo['options'])) {
                $this->options = array_merge(
                    $this->options,
                    $columnInfo['options']
                );
                unset($columnInfo['options']);
            }

            if ($this->isFillable($columnInfo)) {
                $this->fillables[$columnName] = $columnInfo;
            } else {
                $this->modifers[$columnName] = $columnInfo;
            }
        }
    }

    public function getTableName()
    {
        return $this->tableName;
    }

    public function getFillables()
    {
        return $this->fillables;
    }

    public function getModifers()
    {
        return $this->modifers;
    }

    public function getTimestampsType()
    {
        return isset($this->modifers['timestamps']) ?
            $this->modifers['timestamps'] :
            null;
    }

    public function getMigrationPath()
    {
        return $this->migrationPath;
    }

    public function getFillableTypes()
    {
        return array_column($this->fillables, 'type');
    }

    public function getModelClass()
    {
        return studly_case(str_singular($this->tableName));
    }

    public function getRelationships()
    {
        return $this->relationships;
    }

    public function setRelationships($relationships)
    {
        $this->relationships = $relationships;
    }

    public function isJunctionTable()
    {
        foreach ($this->relationships as $relation) {
            if ($relation['relationship'] == 'junction-table') {
                return true;
            }
        }

        return false;
    }

    public function getForeignKeyTableName($columnName)
    {
        return str_plural(rtrim($columnName, '_id'));
    }

    public function getForeignKeyRelationship($columnName)
    {
        $expectedForeignTableName = $this->getForeignKeyTableName($columnName);

        foreach ($this->relationships as $relationship) {
            if ($expectedForeignTableName == $relationship['foreign_table']) {
                return $relationship['relationship'];
            }
        }

        return false;
    }

    public function isForeignKey($columnName)
    {
        $expectedForeignTableName = $this->getForeignKeyTableName($columnName);

        $allForeignTableNames = array_column(
            $this->relationships,
            'foreign_table'
        );

        return in_array($expectedForeignTableName, $allForeignTableNames);
    }

    public function getManyToManyRelationship()
    {
        $manyToManyRelationship = [];
        foreach ($this->relationships as $relationship) {
            if ($relationship['relationship'] == 'many-to-many') {
                $manyToManyRelationship[] = $relationship;
            }
        }

        return $manyToManyRelationship;
    }

    public function getOption($optionName)
    {
        if (isset($this->options[$optionName])) {
            return $this->options[$optionName];
        }

        return null;
    }

    private function isFillable($columnInfo)
    {
        return isset($columnInfo['type']);
    }
}