<?php

namespace GreenPlate\Crud\CrudCreator\ModelInfoConverter;

class ModelInfoConverter
{
    public function convert($migrationInfo)
    {

        $modelInfos = [];
        foreach ($migrationInfo as $tableName => $info) {
            $modelInfos[] = new ModelInfo($tableName, $info);
        }

        $tableRelationShipInfos =
            (new TableRelationshipAnalyzer($migrationInfo))->analyze();

        foreach ($modelInfos as $index => $modelInfo) {
            $tableName = $modelInfo->getTableName();

            if(isset($tableRelationShipInfos[$tableName])) {
                $modelInfo->setRelationships($tableRelationShipInfos[$tableName]);
            }
        }

        return $modelInfos;
    }
}