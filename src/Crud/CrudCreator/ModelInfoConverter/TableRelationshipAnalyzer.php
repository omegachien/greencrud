<?php

namespace GreenPlate\Crud\CrudCreator\ModelInfoConverter;

class TableRelationshipAnalyzer
{
    /**
     * Migration info array.
     *
     * @var array
     */
    private $migrationInfo;

    /**
     * A list of all table names in $migrationInfo
     *
     * @var array
     */
    private $tableNameList;

    /**
     * Array storing the relationship information for all tables.
     *
     * @var array
     */
    private $tableRelationShipInfo = [];

    /**
     * Initialize $migrationInfo property of the class.
     *
     * @param array $migrationInfo
     */
    public function __construct($migrationInfo)
    {
        $this->migrationInfo = $migrationInfo;
    }

    /**
     * Trigger scan for 'Many to Many' and 'Many to One' relationship,
     * then populate $tableRelationShipInfo
     *
     * @return array
     */
    public function analyze()
    {
        $this->scanForManyToMany();
        $this->scanForManyToOne();

        return $this->tableRelationShipInfo;
    }

    /**
     * Scan for 'Many to Many' relationship in $migrationInfo
     *
     * Many to Many table is determined by,
     * 1) Find a possible junction table
     * 2) Check if the 2 tables that the junction table is referring
     *    to are inside our migrationInfo
     *
     * @return void
     */
    private function scanForManyToMany()
    {
        foreach ($this->migrationInfo as $tableName => $tableInfo) {
            $junctionTableStatus = $this->junctionTableCheck($tableName);

            // 1) Find a possible junction table
            if ($junctionTableStatus !== false) {

                // 2) Check if the 2 tables that the junction table is referring
                // to are inside our migrationInfo
                $foreignTableNames = [];
                foreach ($junctionTableStatus as $foreignTableName) {
                    $foreignTableNames[] = str_plural($foreignTableName);
                }
                list($table1, $table2) = $foreignTableNames;
                if (isset($this->migrationInfo[$table1]) &&
                    isset($this->migrationInfo[$table2])) {
                    $this->tableRelationShipInfo[$tableName][] = [
                        'relationship' => 'junction-table',
                    ];
                    $this->tableRelationShipInfo[$table1][] = [
                        'relationship' => 'many-to-many',
                        'foreign_table' => $table2,
                    ];
                    $this->tableRelationShipInfo[$table2][] = [
                        'relationship' => 'many-to-many',
                        'foreign_table' => $table1,
                    ];
                }
            }
        }
    }

    /**
     * Scan for 'Many to One' and inversely 'One to Many'
     * relationship altogether in $migrationInfo
     *
     * 1) Table must not be a junction table
     * 2) Table has a foreign key that is referring to another
     *    table in our migrationInfo
     *
     * @return void
     */
    private function scanForManyToOne()
    {
        foreach ($this->migrationInfo as $tableName => $tableInfo) {
            // 1) Table must not be a junction table
            if ($this->junctionTableCheck($tableName) === false) {

                // 2) Table has a foreign key that is referring to another
                // table in our migrationInfo
                $foreignKeys = $this->getTableForeignKey($tableName);
                foreach ($foreignKeys as $foreignKey) {
                    $foreignTable = str_plural(rtrim($foreignKey, '_id'));
                    if (isset($this->migrationInfo[$foreignTable])) {
                        $this->tableRelationShipInfo[$tableName][] = [
                            'relationship' => 'many-to-one',
                            'foreign_table' => $foreignTable,
                        ];
                        $this->tableRelationShipInfo[$foreignTable][] = [
                            'relationship' => 'one-to-many',
                            'foreign_table' => $tableName,
                        ];
                    }
                }
            }
        }
    }

    /**
     * Possible junction table is determined by:
     * 1) Have 2 foreign key
     * 2) The table name is constructed by the 2 foreign keys
     *    name without '_id'
     *
     * @param  string $tableName
     * @return boolean|array
     */
    private function junctionTableCheck($tableName)
    {
        // 1) Have 2 foreign key
        $foreignKeys = $this->getTableForeignKey($tableName);
        if (count($foreignKeys) < 2) {
            return false;
        }

        $matchTableNameCount = 0;
        foreach ($foreignKeys as &$foreignKey) {
            $foreignKey = rtrim($foreignKey, '_id');
        }
        unset($foreignKey);

        // create all permutations of the foreign key combined
        // to see if it matches its table name
        $foreignKeysLength = count($foreignKeys);
        for ($i=0; $i < $foreignKeysLength; $i++) {
            for ($j=0; $j < $foreignKeysLength; $j++) {
                if ($i != $j) {
                    $possibleTableName =
                        $foreignKeys[$i] . '_' . $foreignKeys[$j];

                    if ($possibleTableName == $tableName) {
                        return [
                            $foreignKeys[$i],
                            $foreignKeys[$j]
                        ];
                    }
                }
            }
        }

        return false;
    }

    private function getTableForeignKey($tablename)
    {
        $foreignKeys = [];
        foreach ($this->migrationInfo[$tablename] as $fieldName => $fieldInfo) {
            if (isset($fieldInfo['foreign_key'])) {
                $foreignKeys[] = $fieldName;
            }
        }

        return $foreignKeys;
    }
}
