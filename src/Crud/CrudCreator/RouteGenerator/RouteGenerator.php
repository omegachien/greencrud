<?php

namespace GreenPlate\Crud\CrudCreator\RouteGenerator;

use GreenPlate\Crud\CrudHelpers;
use GreenPlate\Helper\Helper;

class RouteGenerator
{
    /**
     * Array storing all the model infos
     *
     * @var array
     */
    protected $modelInfos;

    /**
     * String directory path
     *
     * @var string
     */
    protected $stubDirectory;

    /**
     * An array storing all the ColumnModelGenerators required
     *
     * @var array
     */
    protected $columnModelGenerators = [];

    /**
     * Initialize ModelGenerator
     *
     * @param array $modelInfos
     */
    public function __construct($modelInfos)
    {
        $this->modelInfos = $modelInfos;
        $this->stubDirectory = __DIR__ . '/stubs';

        $this->columnRouteGenerators =
            CrudHelpers::initializeColumnRouteGenerator($modelInfos);
    }

    public function generate()
    {
        $backendRoutePath = base_path('routes/backend-routes.php');
//        Helper::createIfNotExists($backendRoutePath);

        $baseRoutesStub = file_get_contents($this->stubDirectory . '/base-backend-routes.stub');

        foreach ($this->modelInfos as $modelInfo) {
            // only generate if it is not a junction table
            if ( ! $modelInfo->isJunctionTable()) {
                $routesStub = $baseRoutesStub;

                $entityFolderName = studly_case(str_singular($modelInfo->getTableName()));
                $controllerClassName = $entityFolderName . 'Controller';
                $entityCamelName = camel_case(str_singular($modelInfo->getTableName()));
                $entityRoutePrefix = strtolower($entityCamelName);
                $entityNamespaceName = Helper::pathToPsr4Namespace($entityFolderName);

                if ( ! file_exists($backendRoutePath)) {
                    file_put_contents($backendRoutePath, '');
                }
                $existingRoutesContent = file_get_contents($backendRoutePath);
                if (strpos($existingRoutesContent, "'" . $entityRoutePrefix . "'") === false) {
                    $additionalRoutes = $this->getAdditionalRoutes($modelInfo);
                    $routesContent = $existingRoutesContent . PHP_EOL .
                        $routesStub . $additionalRoutes;

                    $routesContent = str_replace('{{~entityRoutePrefix~}}', $entityRoutePrefix, $routesContent);
                    $routesContent = str_replace('{{~entityNamespaceName~}}', $entityNamespaceName, $routesContent);
                    $routesContent = str_replace('{{~contollerClassName~}}', $controllerClassName, $routesContent);
                    $routesContent = str_replace('{{~entityCamelName~}}', $entityCamelName, $routesContent);

                    file_put_contents($backendRoutePath, $routesContent);
                }
            }
        }
    }

    private function getAdditionalRoutes($modelInfo)
    {
        $routes = '';

        foreach ($modelInfo->getFillables() as $fieldName => $fieldInfo) {
            $type = $fieldInfo['type'];

            $extraRoute = $this->columnRouteGenerators[$type]
                ->getAdditionalRoutesStub($fieldName);
            if ( ! empty($extraRoute)) {
                $routes .= $extraRoute . PHP_EOL;
            }

        }
        $routes = rtrim($routes, PHP_EOL);

        return $routes;
    }

}
