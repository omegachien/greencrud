<?php

namespace GreenPlate\Crud\CrudCreator\ControllerGenerator;

use GreenPlate\Crud\CrudHelpers;
use GreenPlate\Helper\Helper;
use Illuminate\Filesystem\Filesystem;

class ControllerGenerator
{

    /**
     * Array storing all the model infos
     *
     * @var array
     */
    protected $modelInfos;

    /**
     * String directory path
     *
     * @var string
     */
    protected $stubDirectory;

    /**
     * An array storing all the ColumnControllerGenerators required
     *
     * @var array
     */
    protected $columnControllerGenerators = [];

    protected $entityStringConstants = [];

    /**
     * Initialize ControllerGenerator
     *
     * @param array $modelInfos
     */
    public function __construct($modelInfos)
    {
        $this->modelInfos = $modelInfos;
        $this->stubDirectory = __DIR__ . '/stubs';

        $this->columnControllerGenerators =
            CrudHelpers::initializeColumnControllerGenerator($modelInfos);
    }

    public function generate()
    {
        $controllerDirectory = base_path('app/Http/Controllers');

        foreach ($this->modelInfos as $modelInfo) {
            // only generates controller if it is not a junction table
            if ( ! $modelInfo->isJunctionTable()) {
                // start with base
                $controllerFileContent =
                    file_get_contents($this->stubDirectory . '/base-controller.stub');

                $this->initEntityStringConstants($modelInfo);

                // Generating {{~entityNamespaceName~}}
                $controllerFileContent = str_replace(
                    '{{~entityNamespaceName~}}',
                    $this->entityStringConstants['entityNamespaceName'],
                    $controllerFileContent
                );

                // Generating {{~controllerClassName~}}
                $controllerFileContent = str_replace(
                    '{{~controllerClassName~}}',
                    $this->entityStringConstants['controllerClassName'],
                    $controllerFileContent
                );

                // Generating {{~namespaceUses~}}
                $controllerFileContent =
                    $this->generateNamespaceUses($controllerFileContent, $modelInfo);

                // Generating {{~viewListAll~}}
                $controllerFileContent =
                    $this->generateViewListAll($controllerFileContent, $modelInfo);

                // Generating {{~viewCreate~}}
                $controllerFileContent =
                    $this->generateViewCreate($controllerFileContent, $modelInfo);

                // Generating {{~postCreate~}}
                $controllerFileContent =
                    $this->generatePostCreate($controllerFileContent, $modelInfo);

                // Generating {{~viewEdit~}}
                $controllerFileContent =
                    $this->generateViewEdit($controllerFileContent, $modelInfo);

                // Generating {{~postEdit~}}
                $controllerFileContent =
                    $this->generatePostEdit($controllerFileContent, $modelInfo);

                // Generating {{~postDelete~}}
                $controllerFileContent =
                    $this->generatePostDelete($controllerFileContent, $modelInfo);

                // Generating {{~additionalMethods~}}
                $controllerFileContent =
                    $this->generateAdditionalMethods($controllerFileContent, $modelInfo);

                // Put contents into file
                $controllerDirectory = base_path('app/Http/Controllers');
                $entityControllerDirectory = $controllerDirectory . '/' . $modelInfo->getModelClass() . '/Backend';
                if ( ! file_exists($entityControllerDirectory)) {
                    mkdir($entityControllerDirectory, 0775, true);
                }

                file_put_contents(
                    $entityControllerDirectory . '/' . $modelInfo->getModelClass() . 'Controller.php',
                    $controllerFileContent
                );
            }
        }
    }

    private function generateNamespaceUses($controllerFileContent, $modelInfo)
    {
        $namespaceUsesArray = [
            'App\Models\\' . $this->entityStringConstants['entityModelClass'],
            'App\Http\Controllers\Controller',
            'DB',
            'Illuminate\Http\Request',
        ];

        foreach ($modelInfo->getFillableTypes() as $type) {
            $typeNamespaceUses = $this->columnControllerGenerators[$type]
                ->getNamespaceUses();
            $namespaceUsesArray = array_merge($namespaceUsesArray, $typeNamespaceUses);
        }

        // Relationship
        foreach ($modelInfo->getRelationships() as $relationshipInfo) {
            if ($relationshipInfo['relationship'] == 'many-to-many' ||
                $relationshipInfo['relationship'] == 'many-to-one'
            ) {

                $foreignEntityNameSingular = studly_case(str_singular(
                    $relationshipInfo['foreign_table']
                ));

                $namespaceUsesArray[] =
                    'App\Models\\' . $foreignEntityNameSingular;
            }
        }

        $namespaceUsesArray = array_unique($namespaceUsesArray);
        sort($namespaceUsesArray);

        $namespaceUsesString = '';
        foreach ($namespaceUsesArray as $namespaceUse) {
            $namespaceUsesString .= 'use ' . $namespaceUse . ';' . PHP_EOL;
        }

        $controllerFileContent = str_replace(
            '{{~entityNamespaceUse~}}',
            $namespaceUsesString,
            $controllerFileContent
        );

        return $controllerFileContent;
    }

    private function generateViewListAll($controllerFileContent, $modelInfo)
    {
        // start with base
        $viewListAllContent =
            file_get_contents($this->stubDirectory . '/view-list-all.stub');

        $viewListAllContent = str_replace(
            '{{~entityCamelNamePlural~}}',
            $this->entityStringConstants['entityCamelNamePlural'],
            $viewListAllContent
        );
        $viewListAllContent = str_replace(
            '{{~entityModelClass~}}',
            $this->entityStringConstants['entityModelClass'],
            $viewListAllContent
        );
        $viewListAllContent = str_replace(
            '{{~entityViewFolderName~}}',
            $this->entityStringConstants['entityViewFolderName'],
            $viewListAllContent
        );

        $controllerFileContent =
            str_replace('{{~viewListAll~}}', $viewListAllContent, $controllerFileContent);

        return $controllerFileContent;
    }

    private function generateViewCreate($controllerFileContent, $modelInfo)
    {
        // start with base
        $viewCreateContent =
            file_get_contents($this->stubDirectory . '/view-create.stub');

        $foreignTableDatasCreateView = '';
        $dataToCreate = [];
        foreach ($modelInfo->getRelationships() as $relationshipInfo) {
            if ($relationshipInfo['relationship'] != 'junction-table') {
                switch ($relationshipInfo['relationship']) {
                    case 'many-to-many':
                    case 'many-to-one':
                        $entityCamelNamePlural =
                            camel_case(str_plural($relationshipInfo['foreign_table']));
                        $modelName =
                            studly_case(str_singular($relationshipInfo['foreign_table']));
                        $foreignTableDatasCreateView .= PHP_EOL . str_repeat(TAB_SPACE, 2) .
                            '$' . $entityCamelNamePlural . ' = ' . $modelName . '::all();';

                        $dataToCreate[] = $entityCamelNamePlural;
                        break;

                    default:
                        break;
                }
            }
        }
        $foreignTableDatasCreateView = empty($foreignTableDatasCreateView) ?
            '' : $foreignTableDatasCreateView . PHP_EOL;

        $dataToCreateView = '';
        if ( ! empty($dataToCreate)) {
            $dataToCreateString = '';
            foreach ($dataToCreate as $entityName) {
                $dataToCreateString .= "'" . $entityName . "', ";
            }
            $dataToCreateString = rtrim($dataToCreateString, ', ');
            $dataToCreateView = ", compact([$dataToCreateString])";
        }

        $viewCreateContent = str_replace(
            '{{~foreignTableDatasCreateView~}}',
            $foreignTableDatasCreateView,
            $viewCreateContent
        );
        $viewCreateContent = str_replace(
            '{{~entityViewFolderName~}}',
            $this->entityStringConstants['entityViewFolderName'],
            $viewCreateContent
        );
        $viewCreateContent = str_replace(
            '{{~dataToCreateView~}}',
            $dataToCreateView,
            $viewCreateContent
        );

        $controllerFileContent =
            str_replace('{{~viewCreate~}}', $viewCreateContent, $controllerFileContent);

        return $controllerFileContent;
    }

    private function generatePostCreate($controllerFileContent, $modelInfo)
    {
        $tableName = $modelInfo->getTableName();

        // start with base
        $postCreateContent =
            file_get_contents($this->stubDirectory . '/post-create.stub');

        $fillableValidationArray = [];
        $createInitializationRoutine = [];
        $createDataProcessing = [];
        $createPostDbRoutine = [];
        $createRollbackRoutine = [];
        foreach ($modelInfo->getFillables() as $fieldName => $fieldInfo) {
            $type = $fieldInfo['type'];

            $typeFillableValidations =
                $this->columnControllerGenerators[$type]
                    ->getFillableValidation($tableName, $fieldName, $fieldInfo);

            $fillableValidationArray =
                array_merge($fillableValidationArray, $typeFillableValidations);

            $createInitializationRoutineStub =
                $this->columnControllerGenerators[$type]
                    ->getCreateInitializationRoutineStub($tableName, $fieldName, $fieldInfo);

            $createInputProcessorsStub =
                $this->columnControllerGenerators[$type]
                    ->getCreateInputProcessorsStub($tableName, $fieldName, $fieldInfo);
            $createPostDbRoutineStub =
                $this->columnControllerGenerators[$type]
                    ->getCreatePostDbRoutineStub($tableName, $fieldName, $fieldInfo);
            $createRollbackRoutineStub =
                $this->columnControllerGenerators[$type]
                    ->getCreateRollbackRoutineStub($tableName, $fieldName, $fieldInfo);

            if ( ! empty($createInitializationRoutineStub)) {
                $createInitializationRoutine[] = $createInitializationRoutineStub;
            }
            if ( ! empty($createInputProcessorsStub)) {
                $createDataProcessing[] = $createInputProcessorsStub;
            }
            if ( ! empty($createPostDbRoutineStub)) {
                $createPostDbRoutine[] = $createPostDbRoutineStub;
            }
            if ( ! empty($createRollbackRoutineStub)) {
                $createRollbackRoutine[] = $createRollbackRoutineStub;
            }
        }
        $createInitializationRoutine =
            array_unique($createInitializationRoutine);
        $createDataProcessing =
            array_unique($createDataProcessing);
        $createPostDbRoutine =
            array_unique($createPostDbRoutine);
        $createRollbackRoutine =
            array_unique($createRollbackRoutine);

        $createInitializationRoutine =
            Helper::stringLinePrefix(
                str_repeat(TAB_SPACE, 2),
                implode(PHP_EOL, $createInitializationRoutine)
            );

        $createDataProcessing =
            Helper::stringLinePrefix(
                str_repeat(TAB_SPACE, 3),
                implode(PHP_EOL, $createDataProcessing)
            );
        $createPostDbRoutine = PHP_EOL .
            Helper::stringLinePrefix(
                str_repeat(TAB_SPACE, 3),
                implode(PHP_EOL, $createPostDbRoutine)
            );
        $createRollbackRoutine =
            Helper::stringLinePrefix(
                str_repeat(TAB_SPACE, 3),
                implode(PHP_EOL, $createRollbackRoutine)
            );

        $fillableValidation = PHP_EOL .
            str_repeat(TAB_SPACE, 2) . '//$this->validate($request, [' . PHP_EOL;
        foreach ($fillableValidationArray as $fieldName => $rule) {
            $fillableValidation .=
                str_repeat(TAB_SPACE, 2) . "//" . TAB_SPACE . "'$fieldName' => '$rule'," . PHP_EOL;
        }
        $fillableValidation .= str_repeat(TAB_SPACE, 2) . '//]);';

        // Relationship
        $createPostCreateRoutine = '';
        foreach ($modelInfo->getRelationships() as $relationshipInfo) {
            if ($relationshipInfo['relationship'] == 'many-to-many') {

                $tableNameSingular = camel_case(str_singular($tableName));
                $foreignEntityNameSingular = camel_case(str_singular(
                    $relationshipInfo['foreign_table']
                ));
                $foreignEntityNamePlural = camel_case(str_plural(
                    $relationshipInfo['foreign_table']
                ));

                $createPostCreateRoutine .= PHP_EOL . str_repeat(TAB_SPACE, 3) .
                    "\${$tableNameSingular}->{$foreignEntityNamePlural}()" .
                    "->sync(\$input['{$foreignEntityNameSingular}_ids']);";

                $createDataProcessing .= PHP_EOL . str_repeat(TAB_SPACE, 3) .
                    "\$input['{$foreignEntityNameSingular}_ids'] = isset(\$input['{$foreignEntityNameSingular}_ids']) ? \$input['{$foreignEntityNameSingular}_ids'] : [];";
            }
        }

        $postCreateContent = str_replace(
            '{{~entityCamelName~}}',
            $this->entityStringConstants['entityCamelName'],
            $postCreateContent
        );
        $postCreateContent = str_replace(
            '{{~entityModelClass~}}',
            $this->entityStringConstants['entityModelClass'],
            $postCreateContent
        );
        $postCreateContent = str_replace(
            '{{~entitySingularHumanReadableName~}}',
            $this->entityStringConstants['entitySingularHumanReadableName'],
            $postCreateContent
        );
        $postCreateContent = str_replace(
            '{{~entityRoutePrefix~}}',
            $this->entityStringConstants['entityRoutePrefix'],
            $postCreateContent
        );

        $postCreateContent = str_replace(
            '{{~fillableValidation~}}',
            $fillableValidation,
            $postCreateContent
        );

        $postCreateContent = str_replace(
            '{{~createInitializationRoutine~}}',
            $createInitializationRoutine,
            $postCreateContent
        );

        $postCreateContent = str_replace(
            '{{~createDataProcessing~}}',
            $createDataProcessing,
            $postCreateContent
        );
        $postCreateContent = str_replace(
            '{{~createPostCreateRoutine~}}',
            $createPostCreateRoutine,
            $postCreateContent
        );
        $postCreateContent = str_replace(
            '{{~createPostDbRoutine~}}',
            $createPostDbRoutine,
            $postCreateContent
        );


        $postCreateContent = str_replace(
            '{{~createRollbackRoutine~}}',
            $createRollbackRoutine,
            $postCreateContent
        );

        $controllerFileContent =
            str_replace('{{~postCreate~}}', $postCreateContent, $controllerFileContent);

        return $controllerFileContent;
    }

    private function generateViewEdit($controllerFileContent, $modelInfo)
    {
        $tableName = $modelInfo->getTableName();

        // start with base
        $viewEditContent =
            file_get_contents($this->stubDirectory . '/view-edit.stub');

        $entityCamelName = $this->entityStringConstants['entityCamelName'];

        // foreign table
        $foreignTableDatasEditView = '';
        $dataToEdit = [$entityCamelName];
        foreach ($modelInfo->getRelationships() as $relationshipInfo) {
            if ($relationshipInfo['relationship'] != 'junction-table') {
                switch ($relationshipInfo['relationship']) {
                    case 'many-to-many':
                    case 'many-to-one':
                        $entityCamelNamePlural =
                            camel_case(str_plural($relationshipInfo['foreign_table']));
                        $modelName =
                            studly_case(str_singular($relationshipInfo['foreign_table']));
                        $foreignTableDatasEditView .= PHP_EOL . str_repeat(TAB_SPACE, 3) .
                            '$' . $entityCamelNamePlural . ' = ' . $modelName . '::all();';

                        $dataToEdit[] = $entityCamelNamePlural;
                        break;

                    default:
                        break;
                }
            }
        }
        $foreignTableDatasEditView = empty($foreignTableDatasEditView) ?
            '' : $foreignTableDatasEditView . PHP_EOL;

        $dataToEditView = '';
        if ( ! empty($dataToEdit)) {
            $dataToEditString = '';
            foreach ($dataToEdit as $entityName) {
                $dataToEditString .= "'" . $entityName . "', ";
            }
            $dataToEditString = rtrim($dataToEditString, ', ');
            $dataToEditView = ", compact([$dataToEditString])";
        }

        $viewEditContent = str_replace(
            '{{~entityCamelName~}}',
            $entityCamelName,
            $viewEditContent
        );
        $viewEditContent = str_replace(
            '{{~entityModelClass~}}',
            $this->entityStringConstants['entityModelClass'],
            $viewEditContent
        );
        $viewEditContent = str_replace(
            '{{~foreignTableDatasEditView~}}',
            $foreignTableDatasEditView,
            $viewEditContent
        );
        $viewEditContent = str_replace(
            '{{~entityRoutePrefix~}}',
            $this->entityStringConstants['entityRoutePrefix'],
            $viewEditContent
        );
        $viewEditContent = str_replace(
            '{{~entityViewFolderName~}}',
            $this->entityStringConstants['entityViewFolderName'],
            $viewEditContent
        );
        $viewEditContent = str_replace(
            '{{~dataToEditView~}}',
            $dataToEditView,
            $viewEditContent
        );

        $controllerFileContent =
            str_replace('{{~viewEdit~}}', $viewEditContent, $controllerFileContent);

        return $controllerFileContent;
    }

    private function generatePostEdit($controllerFileContent, $modelInfo)
    {
        $tableName = $modelInfo->getTableName();

        // start with base
        $postEditContent =
            file_get_contents($this->stubDirectory . '/post-edit.stub');

        $fillableValidationArray = [];
        $editInitializationRoutine = [];
        $editDataProcessing = [];
        $editPostDbRoutine = [];
        $editRollbackRoutine = [];
        foreach ($modelInfo->getFillables() as $fieldName => $fieldInfo) {
            $type = $fieldInfo['type'];

            $typeFillableValidations =
                $this->columnControllerGenerators[$type]
                    ->getFillableValidation($tableName, $fieldName, $fieldInfo);
            $fillableValidationArray =
                array_merge($fillableValidationArray, $typeFillableValidations);

            $editInitializationRoutineStub =
                $this->columnControllerGenerators[$type]
                    ->getEditInitializationRoutineStub($tableName, $fieldName, $fieldInfo);
            $editInputProcessorsStub =
                $this->columnControllerGenerators[$type]
                    ->getEditInputProcessorsStub($tableName, $fieldName, $fieldInfo);
            $editPostDbRoutineStub =
                $this->columnControllerGenerators[$type]
                    ->getEditPostDbRoutineStub($tableName, $fieldName, $fieldInfo);
            $editRollbackRoutineStub =
                $this->columnControllerGenerators[$type]
                    ->getEditRollbackRoutineStub($tableName, $fieldName, $fieldInfo);

            if ( ! empty($editInitializationRoutineStub)) {
                $editInitializationRoutine[] =
                    $editInitializationRoutineStub;
            }
            if ( ! empty($editInputProcessorsStub)) {
                $editDataProcessing[] =
                    $editInputProcessorsStub;
            }
            if ( ! empty($editPostDbRoutineStub)) {
                $editPostDbRoutine[] =
                    $editPostDbRoutineStub;
            }
            if ( ! empty($editRollbackRoutineStub)) {
                $editRollbackRoutine[] =
                    $editRollbackRoutineStub;
            }
        }
        $editInitializationRoutine = array_unique($editInitializationRoutine);
        $editDataProcessing = array_unique($editDataProcessing);
        $editPostDbRoutine = array_unique($editPostDbRoutine);
        $editRollbackRoutine = array_unique($editRollbackRoutine);

        $editInitializationRoutine = PHP_EOL .
            Helper::stringLinePrefix(
                str_repeat(TAB_SPACE, 2),
                implode(PHP_EOL, $editInitializationRoutine)
            );
        $editDataProcessing = PHP_EOL .
            Helper::stringLinePrefix(
                str_repeat(TAB_SPACE, 3),
                implode(PHP_EOL, $editDataProcessing)
            );
        $editPostDbRoutine = PHP_EOL .
            Helper::stringLinePrefix(
                str_repeat(TAB_SPACE, 3),
                implode(PHP_EOL, $editPostDbRoutine)
            );
        $editRollbackRoutine = PHP_EOL .
            Helper::stringLinePrefix(
                str_repeat(TAB_SPACE, 3),
                implode(PHP_EOL, $editRollbackRoutine)
            );

        $fillableValidation = PHP_EOL .
            str_repeat(TAB_SPACE, 2) . '//$this->validate($request, [' . PHP_EOL;
        foreach ($fillableValidationArray as $fieldName => $rule) {
            $fillableValidation .=
                str_repeat(TAB_SPACE, 2) . "//" . TAB_SPACE . "'$fieldName' => '$rule'," . PHP_EOL;
        }
        $fillableValidation .= str_repeat(TAB_SPACE, 2) . '//]);' . PHP_EOL;

        // Relationship
        $editPostUpdateRoutine = '';
        foreach ($modelInfo->getRelationships() as $relationshipInfo) {
            if ($relationshipInfo['relationship'] == 'many-to-many') {

                $tableNameSingular = camel_case(str_singular($tableName));
                $foreignEntityNameSingular = camel_case(str_singular(
                    $relationshipInfo['foreign_table']
                ));
                $foreignEntityNamePlural = camel_case(str_plural(
                    $relationshipInfo['foreign_table']
                ));

                $editPostUpdateRoutine .= PHP_EOL . str_repeat(TAB_SPACE, 3) .
                    "\${$tableNameSingular}->{$foreignEntityNamePlural}()" .
                    "->sync(\$input['{$foreignEntityNameSingular}_ids']);";

                $editDataProcessing .= PHP_EOL . str_repeat(TAB_SPACE, 3) .
                    "\$input['{$foreignEntityNameSingular}_ids'] = isset(\$input['{$foreignEntityNameSingular}_ids']) ? \$input['{$foreignEntityNameSingular}_ids'] : [];";
            }
        }

        $postEditContent = str_replace(
            '{{~entityCamelName~}}',
            $this->entityStringConstants['entityCamelName'],
            $postEditContent
        );
        $postEditContent = str_replace(
            '{{~entityRoutePrefix~}}',
            $this->entityStringConstants['entityRoutePrefix'],
            $postEditContent
        );
        $postEditContent = str_replace(
            '{{~entitySingularHumanReadableName~}}',
            $this->entityStringConstants['entitySingularHumanReadableName'],
            $postEditContent
        );
        $postEditContent = str_replace(
            '{{~entityModelClass~}}',
            $this->entityStringConstants['entityModelClass'],
            $postEditContent
        );
        $postEditContent = str_replace(
            '{{~fillableValidation~}}',
            $fillableValidation,
            $postEditContent
        );
        $postEditContent = str_replace(
            '{{~editInitializationRoutine~}}',
            $editInitializationRoutine,
            $postEditContent
        );
        $postEditContent = str_replace(
            '{{~editDataProcessing~}}',
            $editDataProcessing,
            $postEditContent
        );
        $postEditContent = str_replace(
            '{{~editPostUpdateRoutine~}}',
            $editPostUpdateRoutine,
            $postEditContent
        );
        $postEditContent = str_replace(
            '{{~editPostDbRoutine~}}',
            $editPostDbRoutine,
            $postEditContent
        );
        $postEditContent = str_replace(
            '{{~editRollbackRoutine~}}',
            $editRollbackRoutine,
            $postEditContent
        );

        $controllerFileContent =
            str_replace('{{~postEdit~}}', $postEditContent, $controllerFileContent);

        return $controllerFileContent;
    }

    private function generatePostDelete($controllerFileContent, $modelInfo)
    {
        $tableName = $modelInfo->getTableName();

        // start with base
        $postDeleteContent =
            file_get_contents($this->stubDirectory . '/post-delete.stub');

        $deleteDataProcessing = '';
        $deletePostDbRoutine = '';
        foreach ($modelInfo->getFillables() as $fieldName => $fieldInfo) {
            $type = $fieldInfo['type'];


            $deleteDataProcessingStub =
                $this->columnControllerGenerators[$type]
                    ->getDeleteDataProcessingStub($tableName, $fieldName, $fieldInfo);
            $deletePostDbRoutineStub =
                $this->columnControllerGenerators[$type]
                    ->getDeletePostDbRoutineStub($tableName, $fieldName, $fieldInfo);


            if ( ! empty($deleteDataProcessingStub)) {
                $deleteDataProcessing .= PHP_EOL . $deleteDataProcessingStub;
            }
            if ( ! empty($deletePostDbRoutineStub)) {
                $deletePostDbRoutine .= PHP_EOL . $deletePostDbRoutineStub;
            }
        }
        $deleteDataProcessing = Helper::stringLinePrefix(
            str_repeat(TAB_SPACE, 3),
            rtrim($deleteDataProcessing, PHP_EOL)
        );
        $deletePostDbRoutine = Helper::stringLinePrefix(
            str_repeat(TAB_SPACE, 3),
            rtrim($deletePostDbRoutine, PHP_EOL)
        );

        $postDeleteContent = str_replace(
            '{{~entityCamelName~}}',
            $this->entityStringConstants['entityCamelName'],
            $postDeleteContent
        );
        $postDeleteContent = str_replace(
            '{{~entityModelClass~}}',
            $this->entityStringConstants['entityModelClass'],
            $postDeleteContent
        );
        $postDeleteContent = str_replace(
            '{{~entitySingularHumanReadableName~}}',
            $this->entityStringConstants['entitySingularHumanReadableName'],
            $postDeleteContent
        );
        $postDeleteContent = str_replace(
            '{{~entityRoutePrefix~}}',
            $this->entityStringConstants['entityRoutePrefix'],
            $postDeleteContent
        );
        $postDeleteContent = str_replace(
            '{{~deleteDataProcessing~}}',
            $deleteDataProcessing,
            $postDeleteContent
        );
        $postDeleteContent = str_replace(
            '{{~deletePostDbRoutine~}}',
            $deletePostDbRoutine,
            $postDeleteContent
        );

        $controllerFileContent =
            str_replace('{{~postDelete~}}', $postDeleteContent, $controllerFileContent);

        return $controllerFileContent;
    }

    private function generateAdditionalMethods($controllerFileContent, $modelInfo)
    {
        $tableName = $modelInfo->getTableName();

        $additionalMethodsContent = '';
        foreach ($modelInfo->getFillables() as $fieldName => $fieldInfo) {
            $type = $fieldInfo['type'];

            $additionalMethodsContent .= PHP_EOL .
                $this->columnControllerGenerators[$type]
                    ->getAdditionalMethodsStub($tableName, $fieldName, $fieldInfo);
        }
        $additionalMethodsContent = rtrim($additionalMethodsContent, PHP_EOL);

        $controllerFileContent =
            str_replace('{{~additionalMethods~}}',
                $additionalMethodsContent,
                $controllerFileContent
            );

        return $controllerFileContent;
    }

    private function initEntityStringConstants($modelInfo)
    {
        $entityFolderName = studly_case(str_singular($modelInfo->getTableName()));
        $entityViewFolderName = strtolower($entityFolderName);
        $controllerClassName = $entityFolderName . 'Controller';
        $entityCamelName = camel_case(str_singular($modelInfo->getTableName()));
        $entityCamelNamePlural = camel_case(str_plural($modelInfo->getTableName()));
        $entityRoutePrefix = strtolower($entityCamelName);
        $entityNamespaceName = Helper::pathToPsr4Namespace($entityFolderName);
        $entitySingularHumanReadableName = ucfirst(str_singular(str_replace('_', ' ', $modelInfo->getTableName())));
        $entityModelClass = $modelInfo->getModelClass();

        $this->entityStringConstants = [
            'entityFolderName'                => $entityFolderName,
            'entityViewFolderName'            => $entityViewFolderName,
            'controllerClassName'             => $controllerClassName,
            'entityCamelName'                 => $entityCamelName,
            'entityCamelNamePlural'           => $entityCamelNamePlural,
            'entityRoutePrefix'               => $entityRoutePrefix,
            'entityNamespaceName'             => $entityNamespaceName,
            'entitySingularHumanReadableName' => $entitySingularHumanReadableName,
            'entityModelClass'                => $entityModelClass,
        ];
    }
}
