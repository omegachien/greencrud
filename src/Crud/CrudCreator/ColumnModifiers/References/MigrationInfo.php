<?php

namespace GreenPlate\Crud\CrudCreator\ColumnModifiers\References;

use GreenPlate\Crud\CrudCreator\MigrationExtractor\BaseMigrationInfo;

class MigrationInfo extends BaseMigrationInfo
{
    /**
     * Toggle timestamps status to true
     *
     * @param array $migrationInfo
     * @param array $arguments
     * @return array
     */
    public function addMigrationInfo($migrationInfo, ...$arguments)
    {
        list($fieldName) = $arguments;

        $migrationInfo[$this->lastFieldName]['foreign_field'] = $fieldName;

        return $migrationInfo;
    }
}
