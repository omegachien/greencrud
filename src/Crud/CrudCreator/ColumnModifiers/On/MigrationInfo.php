<?php

namespace GreenPlate\Crud\CrudCreator\ColumnModifiers\On;

use GreenPlate\Crud\CrudCreator\MigrationExtractor\BaseMigrationInfo;

class MigrationInfo extends BaseMigrationInfo
{
    /**
     * Toggle timestamps status to true
     *
     * @param array $migrationInfo
     * @param array $arguments
     * @return array
     */
    public function addMigrationInfo($migrationInfo, ...$arguments)
    {
        list($tableName) = $arguments;

        $migrationInfo[$this->lastFieldName]['foreign_table'] = $tableName;

        return $migrationInfo;
    }
}
