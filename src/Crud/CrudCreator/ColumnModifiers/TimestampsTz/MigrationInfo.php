<?php

namespace GreenPlate\Crud\CrudCreator\ColumnModifiers\TimestampsTz;

use GreenPlate\Crud\CrudCreator\MigrationExtractor\BaseMigrationInfo;

class MigrationInfo extends BaseMigrationInfo
{
    /**
     * Toggle timestamps status to true
     *
     * @param array $migrationInfo
     * @param array $arguments
     * @return array
     */
    public function addMigrationInfo($migrationInfo, ...$arguments)
    {
        $migrationInfo['timestamps'] = 'timestampsTz';

        return $migrationInfo;
    }
}
