<?php

namespace GreenPlate\Crud\CrudCreator\ColumnModifiers\Foreign;

use GreenPlate\Crud\CrudCreator\MigrationExtractor\BaseMigrationInfo;

class MigrationInfo extends BaseMigrationInfo
{
    /**
     * Toggle timestamps status to true
     *
     * @param array $migrationInfo
     * @param array $arguments
     * @return array
     */
    public function addMigrationInfo($migrationInfo, ...$arguments)
    {
        list($fieldName) = $arguments;

        $migrationInfo[$fieldName]['foreign_key'] = true;
        $this->lastFieldName = $fieldName;

        return $migrationInfo;
    }
}
