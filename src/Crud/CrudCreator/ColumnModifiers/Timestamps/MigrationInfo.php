<?php

namespace GreenPlate\Crud\CrudCreator\ColumnModifiers\Timestamps;

use GreenPlate\Crud\CrudCreator\MigrationExtractor\BaseMigrationInfo;

class MigrationInfo extends BaseMigrationInfo
{
    /**
     * Toggle timestamps status to true
     *
     * @param array $migrationInfo
     * @param array $arguments
     * @return array
     */
    public function addMigrationInfo($migrationInfo, ...$arguments)
    {
        $migrationInfo['timestamps'] = 'timestamps';

        return $migrationInfo;
    }
}
