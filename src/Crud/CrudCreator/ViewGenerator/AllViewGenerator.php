<?php

namespace GreenPlate\Crud\CrudCreator\ViewGenerator;

use GreenPlate\Crud\CrudHelpers;

class AllViewGenerator
{
    /**
     * Array storing all the model infos
     *
     * @var array
     */
    protected $modelInfos;

    /**
     * String directory path
     *
     * @var string
     */
    protected $stubDirectory;

    /**
     * An array storing all the ColumnViewGenerators required
     *
     * @var array
     */
    protected $columnViewGenerators = [];

    protected $entityStringConstants = [];

    /**
     * Initialize ViewGenerator
     *
     * @param array $modelInfos
     */
    public function __construct($modelInfos, $columnViewGenerators)
    {
        $this->modelInfos = $modelInfos;
        $this->stubDirectory = __DIR__ . '/stubs';

        $this->columnViewGenerators = $columnViewGenerators;
    }

    public function generate()
    {
        foreach ($this->modelInfos as $modelInfo) {
            // only generate if it is not a junction table
            if (!$modelInfo->isJunctionTable()) {
                $this->initEntityStringConstants($modelInfo);

                // start with base
                $viewAllContent =
                    file_get_contents($this->stubDirectory . '/view.stub');

                $viewAllContent = str_replace(
                    '{{~entityRoutePrefix~}}',
                    $this->entityStringConstants['entityRoutePrefix'],
                    $viewAllContent
                );
                $viewAllContent = str_replace(
                    '{{~entityHumanReadableNameSingular~}}',
                    $this->entityStringConstants['entityHumanReadableNameSingular'],
                    $viewAllContent
                );
                $viewAllContent = str_replace(
                    '{{~entityHumanReadableName~}}',
                    $this->entityStringConstants['entityHumanReadableName'],
                    $viewAllContent
                );
                $viewAllContent = str_replace(
                    '{{~entityCamelNamePlural~}}',
                    $this->entityStringConstants['entityCamelNamePlural'],
                    $viewAllContent
                );
                $viewAllContent = str_replace(
                    '{{~entityCamelName~}}',
                    $this->entityStringConstants['entityCamelName'],
                    $viewAllContent
                );
                $viewAllContent = str_replace(
                    '{{~entityTableHeader~}}',
                    $this->entityStringConstants['entityTableHeader'],
                    $viewAllContent
                );
                $viewAllContent = str_replace(
                    '{{~entityTableRow~}}',
                    $this->entityStringConstants['entityTableRow'],
                    $viewAllContent
                );

                $viewDirectory = base_path('resources/views');
                $entityViewDirectory =$viewDirectory . '/' .
                    $this->entityStringConstants['entityViewFolderName'];
                $entityViewBackendDirectory = $entityViewDirectory . '/backend';
                if (!file_exists($entityViewBackendDirectory)) {
                    mkdir($entityViewBackendDirectory, 0775, true);
                }
                file_put_contents(
                    $entityViewBackendDirectory . '/view.blade.php',
                    $viewAllContent
                );
            }
        }
    }

    private function initEntityStringConstants($modelInfo)
    {
        $entityViewFolderName =
            strtolower(studly_case(str_singular($modelInfo->getTableName())));
        $entityCamelName =
            camel_case(str_singular($modelInfo->getTableName()));
        $entityCamelNamePlural =
            camel_case(str_plural($modelInfo->getTableName()));
        $entityHumanReadableName =
            ucfirst(str_replace('_', ' ', $modelInfo->getTableName()));
        $entityHumanReadableNameSingular =
            str_singular(ucfirst(str_replace('_', ' ', $modelInfo->getTableName())));
        $entityRoutePrefix = strtolower($entityCamelName);

        $entityTableHeader = '';
        foreach ($modelInfo->getFillables() as $fieldName => $fieldInfo) {
            $entityTableHeader .=
                str_repeat(TAB_SPACE, 3) . '<th>' .
                ucfirst(str_replace('_', ' ', $fieldName)) .
                '</th>' . PHP_EOL;
        }
        $entityTableHeader = rtrim($entityTableHeader, PHP_EOL);

        $entityTableRow = '';
        foreach ($modelInfo->getFillables() as $fieldName => $fieldInfo) {
            $entityTableRow .=
                str_repeat(TAB_SPACE, 4) . "<td>{{\$$entityCamelName->$fieldName}}</td>" . PHP_EOL;
        }
        $entityTableRow = rtrim($entityTableRow, PHP_EOL);

        $this->entityStringConstants = [
            'entityViewFolderName' => $entityViewFolderName,
            'entityCamelName' => $entityCamelName,
            'entityCamelNamePlural' => $entityCamelNamePlural,
            'entityHumanReadableName' => $entityHumanReadableName,
            'entityHumanReadableNameSingular' => $entityHumanReadableNameSingular,
            'entityRoutePrefix' => $entityRoutePrefix,
            'entityTableHeader' => $entityTableHeader,
            'entityTableRow' => $entityTableRow,
        ];
    }
}