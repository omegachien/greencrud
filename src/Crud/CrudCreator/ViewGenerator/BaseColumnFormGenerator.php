<?php

namespace GreenPlate\Crud\CrudCreator\ViewGenerator;

class BaseColumnFormGenerator
{
    public function getOldValueStub($tableName, $columnName, $columnInfo)
    {
        $stubPath = __DIR__ . '/stubs/old-value-default.stub';

        $oldValueDefaultStub = file_get_contents($stubPath);

        $fieldSingularCamelName = camel_case(str_singular($columnName));
        $entityCamelName = camel_case(str_singular($tableName));

        $oldValueStub = str_replace('{{~fieldSingularCamelName~}}', $fieldSingularCamelName, $oldValueDefaultStub);
        $oldValueStub = str_replace('{{~fieldName~}}', $columnName, $oldValueStub);
        $oldValueStub = str_replace('{{~entityCamelName~}}', $entityCamelName, $oldValueStub);

        return $oldValueStub;
    }

    public function getAfterPartialForm($tableName, $columnName, $columnInfo)
    {
        return '';
    }

    public function getExtraJavascript($tableName, $columnName, $columnInfo)
    {
        return '';
    }

    public function getCustomCss($tableName, $columnName, $columnInfo)
    {
        return '';
    }

    public function getFormInputStub($tableName, $columnName, $columnInfo)
    {
        return '';
    }

    public function getFormJavascriptsIncludeStub($tableName, $columnName, $columnInfo)
    {
        return '';
    }

    public function getFormJavascriptsStub($tableName, $columnName, $columnInfo)
    {
        return '';
    }
}
