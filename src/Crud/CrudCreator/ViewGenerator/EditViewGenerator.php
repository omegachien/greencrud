<?php

namespace GreenPlate\Crud\CrudCreator\ViewGenerator;

use GreenPlate\Crud\CrudHelpers;

class EditViewGenerator
{
    /**
     * Array storing all the model infos
     *
     * @var array
     */
    protected $modelInfos;

    /**
     * String directory path
     *
     * @var string
     */
    protected $stubDirectory;

    /**
     * An array storing all the ColumnViewGenerators required
     *
     * @var array
     */
    protected $columnViewGenerators = [];

    protected $entityStringConstants = [];

    /**
     * Initialize ViewGenerator
     *
     * @param array $modelInfos
     */
    public function __construct($modelInfos, $columnViewGenerators)
    {
        $this->modelInfos = $modelInfos;
        $this->stubDirectory = __DIR__ . '/stubs';

        $this->columnViewGenerators = $columnViewGenerators;
    }

    public function generate()
    {
        foreach ($this->modelInfos as $modelInfo) {
            // only generate if it is not a junction table
            if (!$modelInfo->isJunctionTable()) {
                $this->initEntityStringConstants($modelInfo);

                // start with base
                $viewEditContent =
                    file_get_contents($this->stubDirectory . '/edit.stub');

                // Generating {{~afterPartialForm~}}
                $viewEditContent =
                    $this->generateAfterPartialForm($viewEditContent, $modelInfo);

                // Generating {{~extraJavascript~}}
                $viewEditContent =
                    $this->generateExtraJavascript($viewEditContent, $modelInfo);

                // Generating {{~customCss~}}
                $viewEditContent =
                    $this->generateCustomCss($viewEditContent, $modelInfo);

                $viewEditContent = str_replace(
                    '{{~entityRoutePrefix~}}',
                    $this->entityStringConstants['entityRoutePrefix'],
                    $viewEditContent
                );
                $viewEditContent = str_replace(
                    '{{~entityCamelName~}}',
                    $this->entityStringConstants['entityCamelName'],
                    $viewEditContent
                );

                $viewEditContent = str_replace(
                    '{{~entityViewFolderName~}}',
                    $this->entityStringConstants['entityViewFolderName'],
                    $viewEditContent
                );

                if ($modelInfo->getOption('isFile')) {
                    $viewEditContent = str_replace('<form class="form-horizontal"', '<form enctype="multipart/form-data" class="form-horizontal"', $viewEditContent);
                }

                $viewDirectory = base_path('resources/views');
                $entityViewDirectory =$viewDirectory . '/' .
                    $this->entityStringConstants['entityViewFolderName'];
                $entityViewBackendDirectory = $entityViewDirectory . '/backend';
                if (!file_exists($entityViewBackendDirectory)) {
                    mkdir($entityViewBackendDirectory, 0775, true);
                }

                file_put_contents(
                    $entityViewBackendDirectory . '/edit.blade.php',
                    $viewEditContent
                );
            }
        }
    }

    private function initEntityStringConstants($modelInfo)
    {
        $entityViewFolderName =
            strtolower(studly_case(str_singular($modelInfo->getTableName())));
        $entityCamelName =
            camel_case(str_singular($modelInfo->getTableName()));
        $entityRoutePrefix = strtolower($entityCamelName);

        $this->entityStringConstants = [
            'entityViewFolderName' => $entityViewFolderName,
            'entityCamelName' => $entityCamelName,
            'entityRoutePrefix' => $entityRoutePrefix,
        ];
    }

    private function generateAfterPartialForm($viewEditContent, $modelInfo)
    {
        $afterPartialForm = '';
        foreach ($modelInfo->getFillables() as $fieldName => $fieldInfo) {
            $type = $fieldInfo['type'];

            $afterPartialForm .=
                $this->columnViewGenerators[$type]
                    ->getAfterPartialForm($modelInfo->getTableName(), $fieldName, $fieldInfo);
        }

        $viewEditContent = str_replace(
            '{{~afterPartialForm~}}',
            $afterPartialForm,
            $viewEditContent
        );

        return $viewEditContent;
    }

    private function generateExtraJavascript($viewEditContent, $modelInfo)
    {
        $extraJavascript = '';
        foreach ($modelInfo->getFillables() as $fieldName => $fieldInfo) {
            $type = $fieldInfo['type'];

            $extraJavascript .=
                $this->columnViewGenerators[$type]
                    ->getExtraJavascript($modelInfo->getTableName(), $fieldName, $fieldInfo);
        }

        $viewEditContent = str_replace(
            '{{~extraJavascript~}}',
            $extraJavascript,
            $viewEditContent
        );

        return $viewEditContent;
    }

    private function generateCustomCss($viewEditContent, $modelInfo)
    {
        $customCss = '';
        foreach ($modelInfo->getFillables() as $fieldName => $fieldInfo) {
            $type = $fieldInfo['type'];

            $customCss .=
                $this->columnViewGenerators[$type]
                    ->getCustomCss($modelInfo->getTableName(), $fieldName, $fieldInfo);
        }

        $viewEditContent = str_replace(
            '{{~customCss~}}',
            $customCss,
            $viewEditContent
        );

        return $viewEditContent;
    }
}