<?php

namespace GreenPlate\Crud\CrudCreator\ViewGenerator;

use GreenPlate\Helper\Helper;

class BackendSidebarGenerator
{
    /**
     * Array storing all the model infos
     *
     * @var array
     */
    protected $modelInfos;

    /**
     * String directory path
     *
     * @var string
     */
    protected $stubDirectory;

    /**
     * Initialize ViewGenerator
     *
     * @param array $modelInfos
     */
    public function __construct($modelInfos, $columnViewGenerators)
    {
        $this->modelInfos = $modelInfos;
        $this->stubDirectory = __DIR__ . '/stubs';
    }

    public function generate()
    {
        $sidebarListItemStub = file_get_contents(
            $this->stubDirectory . '/sidebar-list-item.stub'
        );

        foreach ($this->modelInfos as $modelInfo) {
            // only generate if it is not a junction table
            if ( ! $modelInfo->isJunctionTable()) {
                $entityCamelName = camel_case(str_singular($modelInfo->getTableName()));
                $entityRoutePrefix = strtolower($entityCamelName);
                $entityHumanReadableName =
                    ucfirst(str_replace('_', ' ', $modelInfo->getTableName()));

                $backendSidebarView =
                    base_path('resources/views/_layout/sidebar-left.blade.php');
                Helper::createIfNotExists('resources/views/_layout');
                if ( ! file_exists($backendSidebarView)) {
                    file_put_contents($backendSidebarView, '');
                }

                $existingSidebarListItemView = file_get_contents($backendSidebarView);
                if (strpos($existingSidebarListItemView, $entityHumanReadableName) === false) {
                    $sidebarListItemView = str_replace('{{~entityRoutePrefix~}}', $entityRoutePrefix, $sidebarListItemStub);
                    $sidebarListItemView = str_replace('{{~entityHumanReadableName~}}', $entityHumanReadableName, $sidebarListItemView);

                    $backendSidebarContent = $existingSidebarListItemView . PHP_EOL . $sidebarListItemView;
                    file_put_contents($backendSidebarView, $backendSidebarContent);
                }

            }
        }
    }
}
