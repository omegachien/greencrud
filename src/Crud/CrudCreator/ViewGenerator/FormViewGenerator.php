<?php

namespace GreenPlate\Crud\CrudCreator\ViewGenerator;

use GreenPlate\Crud\CrudHelpers;
use GreenPlate\Helper\Helper;

class FormViewGenerator
{
    /**
     * Array storing all the model infos
     *
     * @var array
     */
    protected $modelInfos;

    /**
     * String directory path
     *
     * @var string
     */
    protected $stubDirectory;

    /**
     * An array storing all the ColumnViewGenerators required
     *
     * @var array
     */
    protected $columnViewGenerators = [];

    protected $entityStringConstants = [];

    /**
     * Initialize ViewGenerator
     *
     * @param array $modelInfos
     */
    public function __construct($modelInfos, $columnViewGenerators)
    {
        $this->modelInfos = $modelInfos;
        $this->stubDirectory = __DIR__ . '/stubs';

        $this->columnViewGenerators = $columnViewGenerators;
    }

    public function generate()
    {
        foreach ($this->modelInfos as $modelInfo) {
            // only generate if it is not a junction table
            if (!$modelInfo->isJunctionTable()) {
                $this->initEntityStringConstants($modelInfo);

                // start with base
                $viewFormContent =
                    file_get_contents($this->stubDirectory . '/form.stub');

                // Generating {{~oldValueInit~}}
                $viewFormContent =
                    $this->generateOldValueInit($viewFormContent, $modelInfo);

                // Generating {{~formInputs~}}
                $viewFormContent =
                    $this->generateFormInputs($viewFormContent, $modelInfo);

                // Generating {{~formJavascripts~}}
                $viewFormContent =
                    $this->generateFormJavascripts($viewFormContent, $modelInfo);

                $viewDirectory = base_path('resources/views');
                $entityViewFolderName =
                    $this->entityStringConstants['entityViewFolderName'];
                $entityViewDirectory = $viewDirectory . '/' . $entityViewFolderName;

                $entityViewPartialDirectory = $entityViewDirectory . '/_partial';
                if (!file_exists($entityViewPartialDirectory)) {
                    mkdir($entityViewPartialDirectory, 0775, true);
                }

                file_put_contents(
                    $entityViewPartialDirectory . '/_' . $entityViewFolderName . '_form.blade.php',
                    $viewFormContent
                );
            }
        }
    }

    private function generateOldValueInit($viewFormContent, $modelInfo)
    {
        // start with base
        $oldValueInitContent = '';

        foreach ($modelInfo->getFillables() as $fieldName => $fieldInfo) {
            $type = $fieldInfo['type'];

            $oldValueInitContent .= PHP_EOL .
                $this->columnViewGenerators[$type]
                     ->getOldValueStub(
                        $modelInfo->getTableName(),
                        $fieldName,
                        $fieldInfo
                    );
        }

        $manyToManyRelationship = $modelInfo->getManyToManyRelationship();
        foreach ($manyToManyRelationship as $relationshipInfo) {
            $oldValueInitContent .= PHP_EOL .
                $this->getManyToManyOldStub(
                    $modelInfo->getTableName(),
                    $relationshipInfo['foreign_table']
                );
        }

        $oldValueInitContent = trim($oldValueInitContent, PHP_EOL);
        $oldValueInitContent = Helper::stringLinePrefix('  ', $oldValueInitContent);
        $viewFormContent = str_replace(
            '{{~oldValueInit~}}',
            $oldValueInitContent,
            $viewFormContent
        );

        return $viewFormContent;
    }

    private function generateFormInputs($viewFormContent, $modelInfo)
    {
        $inputManyToOneStub = file_get_contents(__DIR__ . '/stubs/relationship/input-many-to-one.stub');

        // start with base
        $formInputsInitContent = '';

        foreach ($modelInfo->getFillables() as $fieldName => $fieldInfo) {
            if ($modelInfo->isForeignKey($fieldName)) {
                $fieldHumanReadableName =  ucfirst(str_replace('_', ' ', $fieldName));
                $fieldSingularCamelName = camel_case(str_singular($fieldName));

                switch ($modelInfo->getForeignKeyRelationship($fieldName)) {
                    case 'many-to-one':
                        $inputManyToOne = $inputManyToOneStub;

                        $foreignCamelPluralTableName = camel_case(str_plural(
                            $modelInfo->getForeignKeyTableName($fieldName)
                        ));
                        $foreignCamelSingularTableName = str_singular($foreignCamelPluralTableName);

                        $inputManyToOne = str_replace('{{~fieldName~}}', $fieldName, $inputManyToOne);
                        $inputManyToOne = str_replace('{{~fieldHumanReadableName~}}', $fieldHumanReadableName, $inputManyToOne);
                        $inputManyToOne = str_replace('{{~fieldSingularCamelName~}}', $fieldSingularCamelName, $inputManyToOne);
                        $inputManyToOne = str_replace('{{~foreignCamelSingularTableName~}}', $foreignCamelSingularTableName, $inputManyToOne);
                        $inputManyToOne = str_replace('{{~foreignCamelPluralTableName~}}', $foreignCamelPluralTableName, $inputManyToOne);

                        $formInputsInitContent .= PHP_EOL . $inputManyToOne;
                        break;
                }
            } else {
                $type = $fieldInfo['type'];

                $formInputsInitContent .= PHP_EOL .
                    $this->columnViewGenerators[$type]
                         ->getFormInputStub(
                            $modelInfo->getTableName(),
                            $fieldName,
                            $fieldInfo
                        );

            }
        }

        $manyToManyRelationship = $modelInfo->getManyToManyRelationship();
        foreach ($manyToManyRelationship as $relationshipInfo) {
            $formInputsInitContent .= PHP_EOL .
                $this->getManyToManyFormInputStub(
                    $relationshipInfo['foreign_table']
                );
        }

        $formInputsInitContent = rtrim($formInputsInitContent, PHP_EOL);

        $viewFormContent = str_replace(
            '{{~formInputs~}}',
            $formInputsInitContent,
            $viewFormContent
        );

        return $viewFormContent;
    }

    private function generateFormJavascripts($viewFormContent, $modelInfo)
    {
        // start with base
        $formJavascriptsContent = '';

        $formJavascriptsInclude = '';
        $formJavascripts = '';
        foreach ($modelInfo->getFillables() as $fieldName => $fieldInfo) {
            $type = $fieldInfo['type'];

            $formJavascriptsIncludeStub =
                $this->columnViewGenerators[$type]
                     ->getFormJavascriptsIncludeStub(
                        $modelInfo->getTableName(),
                        $fieldName,
                        $fieldInfo
                    );
            $formJavascriptsStub =
                $this->columnViewGenerators[$type]
                     ->getFormJavascriptsStub(
                        $modelInfo->getTableName(),
                        $fieldName,
                        $fieldInfo
                    );

            if (!empty($formJavascriptsIncludeStub)) {
                $formJavascriptsInclude .=  '  ' . $formJavascriptsIncludeStub . PHP_EOL;
            }
            if (!empty($formJavascriptsStub)) {
                $formJavascripts .= str_repeat('  ', 2) . $formJavascriptsStub. PHP_EOL;
            }
        }

        $manyToManyRelationship = $modelInfo->getManyToManyRelationship();
        foreach ($manyToManyRelationship as $relationshipInfo) {
            $formJavascripts .= Helper::stringLinePrefix(
                str_repeat('  ', 2),
                $this->getManyToManyJavascriptStub($relationshipInfo['foreign_table']) . PHP_EOL
            );
        }

        if (!empty($formJavascripts) || !empty($formJavascriptsInclude)) {
            // start with base
            $formJavascriptsContent =
                file_get_contents(__DIR__ . '/stubs/form-javascript.stub');

            $formJavascriptsContent = str_replace(
                '{{~formJavascriptsInclude~}}',
                $formJavascriptsInclude,
                $formJavascriptsContent
            );
            $formJavascriptsContent = str_replace(
                '{{~formJavascripts~}}',
                $formJavascripts,
                $formJavascriptsContent
            );

            // prepend EOL if javascript is not empty
            $formJavascriptsContent =
                empty($formJavascriptsContent) ?
                '' :
                PHP_EOL . PHP_EOL . $formJavascriptsContent;
        }

        $viewFormContent = str_replace(
            '{{~formJavascripts~}}',
            $formJavascriptsContent,
            $viewFormContent
        );

        return $viewFormContent;
    }

    private function initEntityStringConstants($modelInfo)
    {
        $entityViewFolderName =
            strtolower(studly_case(str_singular($modelInfo->getTableName())));
        $entityCamelName =
            camel_case(str_singular($modelInfo->getTableName()));
        $entityRoutePrefix = strtolower($entityCamelName);

        $this->entityStringConstants = [
            'entityViewFolderName' => $entityViewFolderName,
            'entityCamelName' => $entityCamelName,
            'entityRoutePrefix' => $entityRoutePrefix,
        ];
    }

    private function getManyToManyOldStub($tableName, $foreignTableName)
    {
        $oldValueManyToManyStub = file_get_contents(__DIR__ . '/stubs/relationship/old-value-many-to-many.stub');

        $foreignFieldName = str_singular($foreignTableName) . '_id';
        $foreignFieldPluralName = str_plural($foreignFieldName);
        $foreignFieldPluralCamelName = camel_case($foreignFieldPluralName);
        $entitySingularCamelName = camel_case(str_singular($tableName));
        $foreignEntityPluralCamelName =  camel_case(str_plural($foreignTableName));

        $oldValueManyToManyStub =
            str_replace(
                '{{~foreignFieldPluralCamelName~}}',
                $foreignFieldPluralCamelName,
                $oldValueManyToManyStub
            );
        $oldValueManyToManyStub =
            str_replace(
                '{{~foreignFieldPluralName~}}',
                $foreignFieldPluralName,
                $oldValueManyToManyStub
            );
        $oldValueManyToManyStub =
            str_replace(
                '{{~entitySingularCamelName~}}',
                $entitySingularCamelName,
                $oldValueManyToManyStub
            );
        $oldValueManyToManyStub =
            str_replace(
                '{{~foreignEntityPluralCamelName~}}',
                $foreignEntityPluralCamelName,
                $oldValueManyToManyStub
            );

        return $oldValueManyToManyStub;
    }

    private function getManyToManyFormInputStub($foreignTableName)
    {
        $inputManyToManyStub =
            file_get_contents(__DIR__ . '/stubs/relationship/input-many-to-many.stub');

        $foreignFieldName = str_singular($foreignTableName) . '_id';
        $foreignPluralFieldName = str_plural($foreignFieldName);
        $foreignFieldHumanReadableName = ucfirst(str_replace('_', ' ', $foreignFieldName));
        $foreignEntitySingularCamelName = camel_case(str_singular($foreignTableName));
        $foreignEntityPluralCamelName = str_plural($foreignEntitySingularCamelName);

        $inputManyToManyStub = str_replace(
            '{{~foreignFieldHumanReadableName~}}',
            $foreignFieldHumanReadableName,
            $inputManyToManyStub
        );
        $inputManyToManyStub = str_replace(
            '{{~foreignPluralFieldName~}}',
            $foreignPluralFieldName,
            $inputManyToManyStub
        );
        $inputManyToManyStub = str_replace(
            '{{~foreignEntityPluralCamelName~}}',
            $foreignEntityPluralCamelName,
            $inputManyToManyStub
        );
        $inputManyToManyStub = str_replace(
            '{{~foreignEntitySingularCamelName~}}',
            $foreignEntitySingularCamelName,
            $inputManyToManyStub
        );

        return $inputManyToManyStub;
    }

    private function getManyToManyJavascriptStub($foreignTableName)
    {
        $javascriptManyToManyStub =
            file_get_contents(__DIR__ . '/stubs/relationship/javascript-many-to-many.stub');

        $foreignFieldName = str_singular($foreignTableName) . '_id';
        $foreignFieldPluralName = str_plural($foreignFieldName);

        $javascriptManyToManyStub =
            str_replace(
                '{{~foreignFieldPluralName~}}',
                $foreignFieldPluralName,
                $javascriptManyToManyStub
            );

        return $javascriptManyToManyStub;
    }
}