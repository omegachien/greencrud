<?php

namespace GreenPlate\Crud\CrudCreator\ViewGenerator;

use GreenPlate\Crud\CrudHelpers;

class ViewGenerator
{
    /**
     * Array storing all the model infos
     *
     * @var array
     */
    protected $modelInfos;

    /**
     * String directory path
     *
     * @var string
     */
    protected $stubDirectory;

    /**
     * An array storing all the ColumnViewGenerators required
     *
     * @var array
     */
    protected $columnViewGenerators = [];

    /**
     * Initialize ViewGenerator
     *
     * @param array $modelInfos
     */
    public function __construct($modelInfos)
    {
        $this->modelInfos = $modelInfos;
        $this->stubDirectory = __DIR__ . '/stubs';

        $this->columnViewGenerators =
            CrudHelpers::initializeColumnViewGenerator($modelInfos);
    }

    public function generate()
    {
        $allViewGenerator =
            (new AllViewGenerator($this->modelInfos, $this->columnViewGenerators));
        $allViewGenerator->generate();

        $createViewGenerator =
            (new CreateViewGenerator($this->modelInfos, $this->columnViewGenerators));
        $createViewGenerator->generate();

        $editViewGenerator =
            (new EditViewGenerator($this->modelInfos, $this->columnViewGenerators));
        $editViewGenerator->generate();

        $formViewGenerator =
            (new FormViewGenerator($this->modelInfos, $this->columnViewGenerators));
        $formViewGenerator->generate();

        $backendSidebarGenerator =
            (new BackendSidebarGenerator($this->modelInfos, $this->columnViewGenerators));
        $backendSidebarGenerator->generate();
    }
}
