<?php

namespace GreenPlate\Crud\CrudCreator\ViewGenerator;

use GreenPlate\Crud\CrudHelpers;

class CreateViewGenerator
{
    /**
     * Array storing all the model infos
     *
     * @var array
     */
    protected $modelInfos;

    /**
     * String directory path
     *
     * @var string
     */
    protected $stubDirectory;

    /**
     * An array storing all the ColumnViewGenerators required
     *
     * @var array
     */
    protected $columnViewGenerators = [];

    protected $entityStringConstants = [];

    /**
     * Initialize ViewGenerator
     *
     * @param array $modelInfos
     */
    public function __construct($modelInfos, $columnViewGenerators)
    {
        $this->modelInfos = $modelInfos;
        $this->stubDirectory = __DIR__ . '/stubs';

        $this->columnViewGenerators = $columnViewGenerators;
    }

    public function generate()
    {
        foreach ($this->modelInfos as $modelInfo) {
            // only generate if it is not a junction table
            if (!$modelInfo->isJunctionTable()) {
                $this->initEntityStringConstants($modelInfo);

                // start with base
                $viewCreateContent =
                    file_get_contents($this->stubDirectory . '/create.stub');

                $viewCreateContent = str_replace(
                    '{{~entityViewFolderName~}}',
                    $this->entityStringConstants['entityViewFolderName'],
                    $viewCreateContent
                );
                $viewCreateContent = str_replace(
                    '{{~entityCamelName~}}',
                    $this->entityStringConstants['entityCamelName'],
                    $viewCreateContent
                );
                $viewCreateContent = str_replace(
                    '{{~entityHumanReadableNameSingular~}}',
                    $this->entityStringConstants['entityHumanReadableNameSingular'],
                    $viewCreateContent
                );
                $viewCreateContent = str_replace(
                    '{{~entityRoutePrefix~}}',
                    $this->entityStringConstants['entityRoutePrefix'],
                    $viewCreateContent
                );

                if ($modelInfo->getOption('isFile')) {
                    $viewCreateContent = str_replace('<form class="form-horizontal"', '<form enctype="multipart/form-data" class="form-horizontal"', $viewCreateContent);
                }

                $viewDirectory = base_path('resources/views');
                $entityViewDirectory =$viewDirectory . '/' .
                    $this->entityStringConstants['entityViewFolderName'];
                $entityViewBackendDirectory = $entityViewDirectory . '/backend';
                if (!file_exists($entityViewBackendDirectory)) {
                    mkdir($entityViewBackendDirectory, 0775, true);
                }
                file_put_contents(
                    $entityViewBackendDirectory . '/create.blade.php',
                    $viewCreateContent
                );
            }
        }
    }

    private function initEntityStringConstants($modelInfo)
    {
        $entityViewFolderName =
            strtolower(studly_case(str_singular($modelInfo->getTableName())));
        $entityCamelName =
            camel_case(str_singular($modelInfo->getTableName()));
        $entityHumanReadableNameSingular =
            str_singular(ucfirst(str_replace('_', ' ', $modelInfo->getTableName())));
        $entityRoutePrefix = strtolower($entityCamelName);

        $this->entityStringConstants = [
            'entityViewFolderName' => $entityViewFolderName,
            'entityCamelName' => $entityCamelName,
            'entityHumanReadableNameSingular' => $entityHumanReadableNameSingular,
            'entityRoutePrefix' => $entityRoutePrefix,
        ];
    }
}