<?php

namespace GreenPlate\Crud\CrudCreator;

use GreenPlate\Crud\CrudCreator\MigrationExtractor\MigrationExtractor;
use GreenPlate\Crud\CrudCreator\MigrationGenerator\MigrationGenerator;
use GreenPlate\Crud\CrudCreator\ModelInfoConverter\ModelInfoConverter;
use GreenPlate\Crud\CrudCreator\ModelGenerator\ModelGenerator;
use GreenPlate\Crud\CrudCreator\ControllerGenerator\ControllerGenerator;
use GreenPlate\Crud\CrudCreator\ViewGenerator\ViewGenerator;
use GreenPlate\Crud\CrudCreator\RouteGenerator\RouteGenerator;

class CrudCreator
{
    /**
     * Create a new migration at the given path.
     *
     * @param  array  $crudMigrationList
     */
    public function create($crudMigrationList)
    {
        // Pass the list of migration files to be scanned and extracted
        $migrationInfo =
            (new MigrationExtractor())->extractInfo($crudMigrationList);

        //here we have already gathered some info based on each column type's MigrationInfo.php
//        dd($migrationInfo);

        // Convert the extracted migration info into modelInfo objects
        $modelInfos = (new ModelInfoConverter())->convert($migrationInfo);

        // Generate the actual migration files from crud migration files
        (new MigrationGenerator())->generate($crudMigrationList, $modelInfos);
        // Generate the Model View and Controller files based on the extracted information
        (new ModelGenerator($modelInfos))->generate();
        (new ControllerGenerator($modelInfos))->generate();
        (new ViewGenerator($modelInfos))->generate();
        (new RouteGenerator($modelInfos))->generate();
    }
}
