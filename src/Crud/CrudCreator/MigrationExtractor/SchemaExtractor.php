<?php

namespace GreenPlate\Crud\CrudCreator\MigrationExtractor;

class SchemaExtractor
{
    /**
     * Scan through new table creation
     *
     * @param  string    $table
     * @param  \Closure  $callback
     * @return \App\Console\Commands\Codegen\Crud\CrudCreator\MigrationExtractor\BlueprintExtractor
     */
    public static function create(&$migrationInfo, $table, \Closure $callback)
    {
        $blueprintExtractor = new BlueprintExtractor();

        $callback($blueprintExtractor);

        $migrationInfo = $migrationInfo + [$table => $blueprintExtractor->getExtractedInfo()];
    }
}
