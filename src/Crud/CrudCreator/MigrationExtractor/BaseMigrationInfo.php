<?php

namespace GreenPlate\Crud\CrudCreator\MigrationExtractor;

abstract class BaseMigrationInfo
{
    protected $lastFieldName = '';

    public function __construct($lastFieldName)
    {
        $this->lastFieldName = $lastFieldName;
    }

    public function getLastFieldName()
    {
        return $this->lastFieldName;
    }

    abstract public function addMigrationInfo($extractedInfo, ...$arguments);
}
