<?php

namespace GreenPlate\Crud\CrudCreator\MigrationExtractor;

use GreenPlate\Crud\CrudHelpers;
use Illuminate\Filesystem\Filesystem;

class BlueprintExtractor
{
    /**
     * The filesystem instance.
     *
     * @var \Illuminate\Filesystem\Filesystem
     */
    private $files;

    /**
     * Stores a list of supported input by scanning
     * through the ColumnTypes folder
     *
     * @var array
     */
    private $supportedColumnTypes = [];

    /**
     * Stores a list of supported input by scanning
     * through the ColumnModifers folder
     *
     * @var array
     */
    private $supportedColumnModifiers = [];

    /**
     * The extracted migration info
     *
     * @var array
     */
    private $extractedInfo = [];

    /**
     * Records the last field name in case chaining
     * input modifier requires this information
     *
     * @var string
     */
    private $lastFieldName = '';

    /**
     * Initialize $this->supportedColumnTypes and $this->supportedColumnModifiers
     * on BlueprintExtractor construct
     *
     */
    public function __construct()
    {
        $this->supportedColumnTypes = CrudHelpers::getSupportedColumnTypes();
        $this->supportedColumnModifiers = CrudHelpers::getSupportedColumnModifiers();
    }

    /**
     * To be invoked by SchemaExtractor after all the migration info
     * has been extracted by BlueprintExtractor
     *
     * @return array
     */
    public function getExtractedInfo()
    {
        return $this->extractedInfo;
    }

    /**
     * This is the function that will be extracting information from
     * different column types and modifier for the table
     *
     * @param  string $name
     * @param  array $args
     * @return BlueprintExtractor
     */
    public function __call($name, $args)
    {
        $isColumnTypes = in_array($name, $this->supportedColumnTypes);
        $isColumnModifiers = in_array($name, $this->supportedColumnModifiers);

        if ($isColumnTypes || $isColumnModifiers) {
            $migrationInfoPath = $isColumnTypes ?
                CrudHelpers::getColumnTypeMigrationInfoPath($name) :
                CrudHelpers::getColumnModifierMigrationInfoPath($name);

            require_once($migrationInfoPath);
            $className = $isColumnTypes ?
                CrudHelpers::getColumnTypeMigrationInfoClass($name) :
                CrudHelpers::getColumnModifierMigrationInfoClass($name);

            $migrationInfo = new $className($this->lastFieldName);

            array_unshift($args, $this->extractedInfo);
            $this->extractedInfo =
                call_user_func_array([$migrationInfo, 'addMigrationInfo'], $args);

            $this->lastFieldName = $migrationInfo->getLastFieldName();
        }

        return $this;
    }
}
