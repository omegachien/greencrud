<?php

namespace GreenPlate\Crud\CrudCreator\MigrationExtractor;

use Illuminate\Filesystem\Filesystem;

class MigrationExtractor
{

    public function extractInfo($crudMigrationList)
    {
        $extractedInfo = [];

        foreach ($crudMigrationList as $migrationClass => $migrationPath) {
            // Modify file to use migration extractor classes, if
            // they are not already using
            $this->toMigrationExtractorClass($migrationPath);

            require_once($migrationPath);

            $migrationInfo = [];

            (new $migrationClass())->up($migrationInfo);

            // Merge array with keys preserved, they should be no table with the same name
            $extractedInfo = $extractedInfo + $migrationInfo;

            // Add migration file path info into the extracted info
            foreach ($extractedInfo as $tableName => $info) {
                if (!isset($info['migrationPath'])) {
                    $extractedInfo[$tableName]['migrationPath'] = $migrationPath;
                }
            }
        }

        return $extractedInfo;
    }

    protected function toMigrationExtractorClass($migrationPath)
    {
        $fileContents = file_get_contents($migrationPath);

        $fileContents = str_replace(
            'use Illuminate\Database\Schema\Blueprint;',
            'use GreenPlate\Crud\CrudCreator\MigrationExtractor\BlueprintExtractor;',
            $fileContents
        );
        $fileContents = str_replace(
            'use Illuminate\Database\Migrations\Migration;',
            'use GreenPlate\Crud\CrudCreator\MigrationExtractor\SchemaExtractor;',
            $fileContents
        );
        $fileContents = str_replace(
            'extends Migration',
            '',
            $fileContents
        );
        $fileContents = str_replace(
            'function up()',
            'function up(&$migrationInfo)',
            $fileContents
        );
        $fileContents = str_replace(
            'Schema::create(',
            'SchemaExtractor::create($migrationInfo, ',
            $fileContents
        );
        $fileContents = str_replace(
            'Blueprint ',
            'BlueprintExtractor ',
            $fileContents
        );

        file_put_contents($migrationPath, $fileContents);
    }
}
