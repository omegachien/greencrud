<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\SmallInteger\ColumnControllerGenerator;

use GreenPlate\Crud\CrudCreator\ColumnTypes\Integer\ColumnControllerGenerator\ColumnControllerGenerator as IntegerColumnControllerGenerator;

class ColumnControllerGenerator extends IntegerColumnControllerGenerator {}
