<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\SmallInteger\ColumnRouteGenerator;

use GreenPlate\Crud\CrudCreator\ColumnTypes\Integer\ColumnRouteGenerator\ColumnRouteGenerator as IntegerColumnRouteGenerator;

class ColumnRouteGenerator extends IntegerColumnRouteGenerator {}
