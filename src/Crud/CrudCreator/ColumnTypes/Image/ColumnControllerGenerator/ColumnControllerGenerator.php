<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\Image\ColumnControllerGenerator;

use GreenPlate\Crud\CrudCreator\ControllerGenerator\BaseColumnControllerGenerator;

class ColumnControllerGenerator extends BaseColumnControllerGenerator
{
    public function getNamespaceUses()
    {
        return ['App\Libraries\UploadFileHandler'];
    }

    public function getFillableValidation($tableName, $columnName, $columnInfo)
    {
        return [];
    }

    public function getCreateInitializationRoutineStub($tableName, $columnName, $columnInfo)
    {
        return file_get_contents(__DIR__ . '/initialization-image.stub');
    }

    public function getCreateInputProcessorsStub($tableName, $columnName, $columnInfo)
    {
        $stubPath = __DIR__ . '/processor-image-create.stub';

        $processorInputCreateImageStub = file_get_contents($stubPath);

        $modelClass = studly_case(str_singular($tableName));
        $modelClassColumnSpinalCaseName = spinal_case(
            $modelClass . '-' . $columnName
        );

        $processorInputCreateImageStub = str_replace(
            '{{~modelClassColumnSpinalCaseName~}}',
            $modelClassColumnSpinalCaseName,
            $processorInputCreateImageStub
        );
        $processorInputCreateImageStub = str_replace(
            '{{~columnName~}}',
            $columnName,
            $processorInputCreateImageStub
        );

        return $processorInputCreateImageStub;
    }

    public function getCreateRollbackRoutineStub($tableName, $columnName, $columnInfo)
    {
        return file_get_contents(__DIR__ . '/rollback-image.stub');
    }

    public function getEditInputProcessorsStub($tableName, $columnName, $columnInfo)
    {
        $stubPath = __DIR__ . '/processor-image-edit.stub';

        $processorInputEditImageStub = file_get_contents($stubPath);

        $entityCamelName = camel_case(str_singular($tableName));
        $columnPascalName = ucfirst(camel_case($columnName));
        $modelClass = studly_case(str_singular($tableName));
        $modelClassColumnSpinalCaseName = spinal_case(
            $modelClass . '-' . $columnName
        );

        $processorInputEditImageStub = str_replace(
            '{{~columnPascalName~}}',
            $columnPascalName,
            $processorInputEditImageStub
        );
        $processorInputEditImageStub = str_replace(
            '{{~entityCamelName~}}',
            $entityCamelName,
            $processorInputEditImageStub
        );
        $processorInputEditImageStub = str_replace(
            '{{~columnName~}}',
            $columnName,
            $processorInputEditImageStub
        );
        $processorInputEditImageStub = str_replace(
            '{{~modelClassColumnSpinalCaseName~}}',
            $modelClassColumnSpinalCaseName,
            $processorInputEditImageStub
        );

        return $processorInputEditImageStub;
    }

    public function getDeleteDataProcessingStub($tableName, $columnName, $columnInfo)
    {
        $stubPath = __DIR__ . '/processor-image-delete.stub';

        $processorInputDeleteImageStub = file_get_contents($stubPath);

        $entityCamelName = camel_case(str_singular($tableName));
        $columnPascalName = ucfirst(camel_case($columnName));

        $processorInputDeleteImageStub = str_replace(
            '{{~columnPascalName~}}',
            $columnPascalName,
            $processorInputDeleteImageStub
        );
        $processorInputDeleteImageStub = str_replace(
            '{{~entityCamelName~}}',
            $entityCamelName,
            $processorInputDeleteImageStub
        );
        $processorInputDeleteImageStub = str_replace(
            '{{~columnName~}}',
            $columnName,
            $processorInputDeleteImageStub
        );

        return $processorInputDeleteImageStub;
    }

    public function getDeletePostDbRoutineStub($tableName, $columnName, $columnInfo)
    {
        $stubPath = __DIR__ . '/post-db-image-edit-delete.stub';

        $postDbImageEditDeleteStub = file_get_contents($stubPath);

        $columnPascalName = ucfirst(camel_case($columnName));

        $imageEditDeletePostDbRoutine = str_replace('{{~columnPascalName~}}',
            $columnPascalName,
            $postDbImageEditDeleteStub
        );

        return $imageEditDeletePostDbRoutine;
    }
}
