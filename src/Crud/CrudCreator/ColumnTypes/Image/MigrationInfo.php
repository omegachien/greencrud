<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\Image;

use GreenPlate\Crud\CrudCreator\MigrationExtractor\BaseMigrationInfo;

class MigrationInfo extends BaseMigrationInfo
{
    /**
     * Adds number type to the migration info
     *
     * @param array $migrationInfo
     * @param array $arguments
     * @return array
     */
    public function addMigrationInfo($migrationInfo, ...$arguments)
    {
        list($fieldName) = $arguments;

        $migrationInfo[$fieldName] = [
            'type' => 'image',
            'options' => [
                'isFile' => true,
            ]
        ];

        return $migrationInfo;
    }
}
