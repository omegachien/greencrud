<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\Image;

use GreenPlate\Crud\CrudCreator\MigrationGenerator\BaseMigrationFileReplacer;

class MigrationFileReplacer extends BaseMigrationFileReplacer
{
    public function replace($fileContents)
    {
        return str_replace('->image', '->string', $fileContents);
    }
}