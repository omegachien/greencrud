<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\Image\ColumnFormGenerator;

use GreenPlate\Crud\CrudCreator\ViewGenerator\BaseColumnFormGenerator;

class ColumnFormGenerator extends BaseColumnFormGenerator
{
    public function getOldValueStub($tableName, $columnName, $columnInfo)
    {
        $stubPath = __DIR__ . '/old-value-image.stub';

        $oldValueImageStub = file_get_contents($stubPath);

        $columnSingularCamelName = camel_case(str_singular($columnName));
        $entityCamelName = camel_case(str_singular($tableName));

        $oldValueImageStub = str_replace(
            '{{~columnSingularCamelName~}}',
            $columnSingularCamelName,
            $oldValueImageStub
        );
        $oldValueImageStub = str_replace(
            '{{~entityCamelName~}}',
            $entityCamelName,
            $oldValueImageStub
        );
        $oldValueImageStub = str_replace(
            '{{~columnName~}}',
            $columnName,
            $oldValueImageStub
        );

        return $oldValueImageStub;
    }

    public function getFormInputStub($tableName, $columnName, $columnInfo)
    {
        $stubPath = __DIR__ . '/input-image.stub';

        $inputImageStub = file_get_contents($stubPath);

        $columnHumanReadableName
            = ucfirst(str_replace('_', ' ', $columnName));
        $columnSingularCamelName = camel_case(str_singular($columnName));

        $inputImageStub = str_replace(
            '{{~columnHumanReadableName~}}',
            $columnHumanReadableName,
            $inputImageStub
        );
        $inputImageStub = str_replace(
            '{{~columnSingularCamelName~}}',
            $columnSingularCamelName,
            $inputImageStub
        );
        $inputImageStub = str_replace(
            '{{~columnName~}}',
            $columnName,
            $inputImageStub
        );

        return $inputImageStub;
    }
}
