<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\Decimal;

use GreenPlate\Crud\CrudCreator\MigrationExtractor\BaseMigrationInfo;

class MigrationInfo extends BaseMigrationInfo
{
    /**
     * Adds decimal type to the migration info
     *
     * @param array $migrationInfo
     * @param array $arguments
     * @return array
     */
    public function addMigrationInfo($migrationInfo, ...$arguments)
    {
        list($fieldName) = $arguments;

        $migrationInfo[$fieldName] = [
            'type' => 'decimal',
        ];

        return $migrationInfo;
    }
}
