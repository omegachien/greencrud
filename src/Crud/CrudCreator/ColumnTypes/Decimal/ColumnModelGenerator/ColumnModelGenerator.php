<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\Decimal\ColumnModelGenerator;

use GreenPlate\Crud\CrudCreator\ColumnTypes\Integer\ColumnModelGenerator\ColumnModelGenerator as IntegerColumnModelGenerator;

class ColumnModelGenerator extends IntegerColumnModelGenerator {}
