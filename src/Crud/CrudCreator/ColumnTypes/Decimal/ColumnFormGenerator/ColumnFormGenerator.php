<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\Decimal\ColumnFormGenerator;

use GreenPlate\Crud\CrudCreator\ColumnTypes\Integer\ColumnFormGenerator\ColumnFormGenerator as IntegerColumnFormGenerator;

class ColumnFormGenerator extends IntegerColumnFormGenerator {}
