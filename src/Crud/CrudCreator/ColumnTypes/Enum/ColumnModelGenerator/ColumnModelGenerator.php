<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\Enum\ColumnModelGenerator;

use GreenPlate\Crud\CrudCreator\ModelGenerator\BaseColumnModelGenerator;

class ColumnModelGenerator extends BaseColumnModelGenerator
{
    public function getHelperStub($tableName, $columnName, $columnInfo)
    {
        $stubPath = __DIR__ . '/helper-enum.stub';

        $helperEnumStub = file_get_contents($stubPath);

        $fieldPluralPascalName = studly_case(str_plural($columnName));
        $enumPossibleValues = '';
        foreach ($columnInfo['enumPossibleItems'] as $enumPossibleValue) {
            $enumPossibleValues .=
                str_repeat(TAB_SPACE, 2) . "'$enumPossibleValue'," . PHP_EOL;
        }
        $enumPossibleValues = rtrim($enumPossibleValues, PHP_EOL);

        $helperEnumStub =
            str_replace('{{~fieldPluralPascalName~}}', $fieldPluralPascalName, $helperEnumStub);
        $helperEnumStub =
            str_replace('{{~enumPossibleValues~}}', $enumPossibleValues, $helperEnumStub);

        return $helperEnumStub;
    }

    public function getMutatorStub($tableName, $columnName, $columnInfo)
    {
        $stubPath = __DIR__ . '/mutator-enum.stub';

        $mutatorEnumStub = file_get_contents($stubPath);

        $columnSingularCamelName = str_singular(camel_case($columnName));
        $columnSingularPascalName = ucfirst($columnSingularCamelName);

        $mutatorEnumStub = str_replace(
            '{{~columnSingularPascalName~}}',
            $columnSingularPascalName,
            $mutatorEnumStub
        );
        $mutatorEnumStub = str_replace(
            '{{~columnSingularCamelName~}}',
            $columnSingularCamelName,
            $mutatorEnumStub
        );
        $mutatorEnumStub = str_replace(
            '{{~columnName~}}',
            $columnName,
            $mutatorEnumStub
        );

        return $mutatorEnumStub;
    }
}
