<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\Enum\ColumnFormGenerator;

use GreenPlate\Crud\CrudCreator\ViewGenerator\BaseColumnFormGenerator;

class ColumnFormGenerator extends BaseColumnFormGenerator
{
    public function getFormInputStub($tableName, $columnName, $columnInfo)
    {
        $stubPath = __DIR__ . '/input-enum.stub';

        $inputLocationStub = file_get_contents($stubPath);

        $modelClass = studly_case(str_singular($tableName));
        $columnHumanReadableName = ucfirst(str_replace('_', ' ', $columnName));
        $columnLowerHumanReadableName = strtolower($columnHumanReadableName);
        $columnSingularCamelName = camel_case(str_singular($columnName));
        $columnSingularPascalName = ucfirst($columnSingularCamelName);
        $columnPluralPascalName = str_plural($columnSingularPascalName);

        $formInputStub =
            str_replace('{{~columnHumanReadableName~}}', $columnHumanReadableName, $inputLocationStub);
        $formInputStub =
            str_replace('{{~columnName~}}', $columnName, $formInputStub);
        $formInputStub =
            str_replace('{{~columnLowerHumanReadableName~}}', $columnLowerHumanReadableName, $formInputStub);
        $formInputStub =
            str_replace('{{~modelClass~}}', $modelClass, $formInputStub);
        $formInputStub =
            str_replace('{{~columnPluralPascalName~}}', $columnPluralPascalName, $formInputStub);
        $formInputStub =
            str_replace('{{~columnSingularPascalName~}}', $columnSingularPascalName, $formInputStub);
        $formInputStub =
            str_replace('{{~columnSingularCamelName~}}', $columnSingularCamelName, $formInputStub);

        return $formInputStub;
    }
}
