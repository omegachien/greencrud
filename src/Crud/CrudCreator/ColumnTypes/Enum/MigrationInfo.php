<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\Enum;

use GreenPlate\Crud\CrudCreator\MigrationExtractor\BaseMigrationInfo;

class MigrationInfo extends BaseMigrationInfo
{
    /**
     * Adds number type to the migration info
     *
     * @param array $migrationInfo
     * @param array $arguments
     * @return array
     */
    public function addMigrationInfo($migrationInfo, ...$arguments)
    {
        if (count($arguments) != 2) {
            throw new \Exception(
                'Invalid number of arguments found for enum.'
            );
        }

        list($fieldName, $items) = $arguments;

        $migrationInfo[$fieldName] = [
            'type' => 'enum',
            'enumPossibleItems' => $items,
        ];

        return $migrationInfo;
    }
}
