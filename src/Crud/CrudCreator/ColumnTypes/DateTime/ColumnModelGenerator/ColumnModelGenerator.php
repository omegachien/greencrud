<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\DateTime\ColumnModelGenerator;

use GreenPlate\Crud\CrudCreator\ModelGenerator\BaseColumnModelGenerator;

class ColumnModelGenerator extends BaseColumnModelGenerator
{
    public function getNamespaceUses()
    {
        return [
            'Carbon\Carbon',
        ];
    }

    public function getMutatorStub($tableName, $columnName, $columnInfo)
    {
        $stubPath = __DIR__ . '/mutator-datetime.stub';

        $mutatorDateTimeStub = file_get_contents($stubPath);

        $columnSingularCamelName = str_singular(camel_case($columnName));
        $columnSingularPascalName = ucfirst($columnSingularCamelName);

        $mutatorDateTimeStub = str_replace(
            '{{~columnSingularPascalName~}}',
            $columnSingularPascalName,
            $mutatorDateTimeStub
        );
        $mutatorDateTimeStub = str_replace(
            '{{~columnSingularCamelName~}}',
            $columnSingularCamelName,
            $mutatorDateTimeStub
        );
        $mutatorDateTimeStub = str_replace(
            '{{~columnName~}}',
            $columnName,
            $mutatorDateTimeStub
        );

        return $mutatorDateTimeStub;
    }

    public function getAccessorStub($tableName, $columnName, $columnInfo)
    {
        $stubPath = __DIR__ . '/accessor-datetime.stub';

        $accessorDateTimeStub = file_get_contents($stubPath);

        $columnSingularCamelName = str_singular(camel_case($columnName));
        $columnSingularPascalName = ucfirst($columnSingularCamelName);

        $accessorDateTimeStub = str_replace(
            '{{~columnSingularPascalName~}}',
            $columnSingularPascalName,
            $accessorDateTimeStub
        );
        $accessorDateTimeStub = str_replace(
            '{{~columnSingularCamelName~}}',
            $columnSingularCamelName,
            $accessorDateTimeStub
        );
        $accessorDateTimeStub = str_replace(
            '{{~columnName~}}',
            $columnName,
            $accessorDateTimeStub
        );

        return $accessorDateTimeStub;
    }
}
