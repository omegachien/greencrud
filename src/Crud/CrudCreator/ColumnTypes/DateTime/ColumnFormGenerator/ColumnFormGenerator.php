<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\DateTime\ColumnFormGenerator;

use GreenPlate\Crud\CrudCreator\ViewGenerator\BaseColumnFormGenerator;

class ColumnFormGenerator extends BaseColumnFormGenerator
{
    public function getFormInputStub($tableName, $columnName, $columnInfo)
    {
        $stubPath = __DIR__ . '/input-datetime.stub';

        $inputTextStub = file_get_contents($stubPath);

        $columnHumanReadableName = ucfirst(str_replace('_', ' ', $columnName));
        $columnSingularCamelName = camel_case(str_singular($columnName));

        $formInputStub = str_replace('{{~columnHumanReadableName~}}', $columnHumanReadableName, $inputTextStub);
        $formInputStub = str_replace('{{~columnName~}}', $columnName, $formInputStub);
        $formInputStub = str_replace('{{~columnSingularCamelName~}}', $columnSingularCamelName, $formInputStub);

        return $formInputStub;
    }

    public function getFormJavascriptsStub($tableName, $columnName, $columnInfo)
    {
        $stubPath = __DIR__ . '/javascript-datetime.stub';

        $javascriptDateTimeStub = file_get_contents($stubPath);

        $javascriptDateTimeStub = str_replace(
            '{{~columnName~}}',
            $columnName,
            $javascriptDateTimeStub
        );

        return $javascriptDateTimeStub;
    }
}
