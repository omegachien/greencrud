<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\MediumInteger\ColumnFormGenerator;

use GreenPlate\Crud\CrudCreator\ColumnTypes\Integer\ColumnFormGenerator\ColumnFormGenerator as IntegerColumnFormGenerator;

class ColumnFormGenerator extends IntegerColumnFormGenerator {}
