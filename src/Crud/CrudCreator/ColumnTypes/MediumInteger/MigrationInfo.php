<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\MediumInteger;

use GreenPlate\Crud\CrudCreator\MigrationExtractor\BaseMigrationInfo;

class MigrationInfo extends BaseMigrationInfo
{
    /**
     * Adds mediumInteger type to the migration info
     *
     * @param array $migrationInfo
     * @param array $arguments
     * @return array
     */
    public function addMigrationInfo($migrationInfo, ...$arguments)
    {
        list($fieldName) = $arguments;

        $migrationInfo[$fieldName] = [
            'type' => 'mediumInteger',
        ];

        return $migrationInfo;
    }
}
