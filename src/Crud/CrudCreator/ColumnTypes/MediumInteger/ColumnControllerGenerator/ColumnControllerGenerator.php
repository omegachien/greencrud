<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\MediumInteger\ColumnControllerGenerator;

use GreenPlate\Crud\CrudCreator\ColumnTypes\Integer\ColumnControllerGenerator\ColumnControllerGenerator as IntegerColumnControllerGenerator;

class ColumnControllerGenerator extends IntegerColumnControllerGenerator {}
