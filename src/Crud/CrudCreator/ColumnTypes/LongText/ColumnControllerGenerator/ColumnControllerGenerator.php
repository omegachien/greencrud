<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\LongText\ColumnControllerGenerator;

use GreenPlate\Crud\CrudCreator\ColumnTypes\Text\ColumnControllerGenerator\ColumnControllerGenerator as TextColumnControllerGenerator;

class ColumnControllerGenerator extends TextColumnControllerGenerator {}
