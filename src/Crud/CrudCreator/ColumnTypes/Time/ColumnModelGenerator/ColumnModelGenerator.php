<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\Time\ColumnModelGenerator;

use GreenPlate\Crud\CrudCreator\ModelGenerator\BaseColumnModelGenerator;

class ColumnModelGenerator extends BaseColumnModelGenerator
{
    public function getNamespaceUses()
    {
        return [
            'Carbon\Carbon',
        ];
    }

    public function getMutatorStub($tableName, $columnName, $columnInfo)
    {
        $stubPath = __DIR__ . '/mutator-time.stub';

        $mutatorTimeStub = file_get_contents($stubPath);

        $columnSingularCamelName = str_singular(camel_case($columnName));
        $columnSingularPascalName = ucfirst($columnSingularCamelName);

        $mutatorTimeStub = str_replace(
            '{{~columnSingularPascalName~}}',
            $columnSingularPascalName,
            $mutatorTimeStub
        );
        $mutatorTimeStub = str_replace(
            '{{~columnSingularCamelName~}}',
            $columnSingularCamelName,
            $mutatorTimeStub
        );
        $mutatorTimeStub = str_replace(
            '{{~columnName~}}',
            $columnName,
            $mutatorTimeStub
        );

        return $mutatorTimeStub;
    }

    public function getAccessorStub($tableName, $columnName, $columnInfo)
    {
        $stubPath = __DIR__ . '/accessor-time.stub';

        $accessorTimeStub = file_get_contents($stubPath);

        $columnSingularCamelName = str_singular(camel_case($columnName));
        $columnSingularPascalName = ucfirst($columnSingularCamelName);

        $accessorTimeStub = str_replace(
            '{{~columnSingularPascalName~}}',
            $columnSingularPascalName,
            $accessorTimeStub
        );
        $accessorTimeStub = str_replace(
            '{{~columnSingularCamelName~}}',
            $columnSingularCamelName,
            $accessorTimeStub
        );
        $accessorTimeStub = str_replace(
            '{{~columnName~}}',
            $columnName,
            $accessorTimeStub
        );

        return $accessorTimeStub;
    }
}
