<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\Url;

use GreenPlate\Crud\CrudCreator\MigrationGenerator\BaseMigrationFileReplacer;

class MigrationFileReplacer extends BaseMigrationFileReplacer
{
    public function replace($fileContents)
    {
        return str_replace('->url', '->string', $fileContents);
    }
}