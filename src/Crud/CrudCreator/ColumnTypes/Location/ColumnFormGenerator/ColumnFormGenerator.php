<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\Location\ColumnFormGenerator;

use GreenPlate\Crud\CrudCreator\ViewGenerator\BaseColumnFormGenerator;

class ColumnFormGenerator extends BaseColumnFormGenerator
{
    public function getOldValueStub($tableName, $columnName, $columnInfo)
    {
        $stubPath = __DIR__ . '/old-value-location.stub';

        $oldValueLocationStub = file_get_contents($stubPath);

        $entityCamelName = camel_case(str_singular($tableName));
        $fieldSingularCamelName = camel_case(str_singular($columnName));

        $oldValueStub = str_replace('{{~fieldSingularCamelName~}}', $fieldSingularCamelName, $oldValueLocationStub);
        $oldValueStub = str_replace('{{~fieldName~}}', $columnName, $oldValueStub);
        $oldValueStub = str_replace('{{~entityCamelName~}}', $entityCamelName, $oldValueStub);

        return $oldValueStub;
    }

    public function getFormInputStub($tableName, $columnName, $columnInfo)
    {
        $stubPath = __DIR__ . '/input-location.stub';

        $inputLocationStub = file_get_contents($stubPath);

        $fieldSingularCamelName = camel_case(str_singular($columnName));
        $fieldHumanReadableName = ucfirst(str_replace('_', ' ', $columnName));

        $formInputStub = str_replace('{{~fieldHumanReadableName~}}', $fieldHumanReadableName, $inputLocationStub);
        $formInputStub = str_replace('{{~fieldName~}}', $columnName, $formInputStub);
        $formInputStub = str_replace('{{~fieldSingularCamelName~}}', $fieldSingularCamelName, $formInputStub);

        return $formInputStub;
    }

    public function getFormJavascriptsIncludeStub($tableName, $columnName, $columnInfo)
    {
        return
            '<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>';
    }

    public function getFormJavascriptsStub($tableName, $columnName, $columnInfo)
    {
        $stubPath = __DIR__ . '/javascript-location.stub';

        $javascriptLocationStub = file_get_contents($stubPath);;

        return str_replace('{{~fieldName~}}', $columnName, $javascriptLocationStub);
    }
}
