<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\Location;

use GreenPlate\Crud\CrudCreator\MigrationGenerator\BaseMigrationFileReplacer;

class MigrationFileReplacer extends BaseMigrationFileReplacer
{
    /**
     * replace all location and change to field have extra lat long field
     * @param $fileContents
     * @return mixed
     */
    public function replace($fileContents)
    {
        $locationMatches = [];
        preg_match_all('/^.*\b->location\b.*$/m', "'$fileContents'", $locationMatches);

        foreach ($locationMatches[0] as $locationMatch) {
            $fieldNameMatch = [];
            preg_match("/->location\(([^()]+)\)/", $locationMatch, $fieldNameMatch);

            $fieldName = trim(trim($fieldNameMatch[1], "'"), '"');
            $quoteChar = substr($fieldNameMatch[1], 0, 1);
            $latFieldName = $quoteChar . $fieldName . '_lat' . $quoteChar;
            $lngFieldName = $quoteChar . $fieldName . '_lng' . $quoteChar;

            $locationRow = str_replace('->location', '->string', $locationMatch);
            $latRow = str_replace('->location', '->double', str_replace($fieldNameMatch[1], $latFieldName, $locationMatch));
            $lngRow = str_replace('->location', '->double', str_replace($fieldNameMatch[1], $lngFieldName, $locationMatch));
            $locationFields = $locationRow . PHP_EOL . $latRow . PHP_EOL . $lngRow;

            $fileContents = str_replace($locationMatch, $locationFields, $fileContents);
        }

        return $fileContents;
    }
}