<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\Location\ColumnModelGenerator;

use GreenPlate\Crud\CrudCreator\ModelGenerator\BaseColumnModelGenerator;

class ColumnModelGenerator extends BaseColumnModelGenerator
{
    public function getFillables($tableName, $columnName, $columnInfo)
    {
        return [
            $columnName,
            $columnName . '_lat',
            $columnName . '_lng',
        ];
    }

    public function getMutatorStub($tableName, $columnName, $columnInfo)
    {
        $stubPath = __DIR__ . '/mutator-location.stub';

        $mutatorLocationStub = file_get_contents($stubPath);

        $columnSingularCamelName = str_singular(camel_case($columnName));
        $columnSingularPascalName = ucfirst($columnSingularCamelName);

        $mutatorLocationStub = str_replace(
            '{{~columnSingularPascalName~}}',
            $columnSingularPascalName,
            $mutatorLocationStub
        );
        $mutatorLocationStub = str_replace(
            '{{~columnSingularCamelName~}}',
            $columnSingularCamelName,
            $mutatorLocationStub
        );
        $mutatorLocationStub = str_replace(
            '{{~columnName~}}',
            $columnName,
            $mutatorLocationStub
        );

        return $mutatorLocationStub;
    }
}
