<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\Location\ColumnControllerGenerator;

use GreenPlate\Crud\CrudCreator\ControllerGenerator\BaseColumnControllerGenerator;

class ColumnControllerGenerator extends BaseColumnControllerGenerator
{
    public function getFillableValidation($tableName, $columnName, $columnInfo)
    {
        return [
            $columnName => 'required',
            $columnName . '_lat' => 'required',
            $columnName . '_lng' => 'required',
        ];
    }
}
