<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\DropZone\ColumnControllerGenerator;

use GreenPlate\Crud\CrudCreator\ControllerGenerator\BaseColumnControllerGenerator;

class ColumnControllerGenerator extends BaseColumnControllerGenerator
{
    public function getNamespaceUses()
    {
        return [];
    }

    public function getFillableValidation($tableName, $columnName, $columnInfo)
    {
        return [
            $columnName => 'required',
        ];
    }

    public function getCreateInitializationRoutineStub($tableName, $columnName, $columnInfo)
    {
        return '';
    }

    public function getCreateInputProcessorsStub($tableName, $columnName, $columnInfo)
    {
        return '';
    }

    public function getCreatePostDbRoutineStub($tableName, $columnName, $columnInfo)
    {
        return '';
    }

    public function getCreateRollbackRoutineStub($tableName, $columnName, $columnInfo)
    {
        return '';
    }

    public function getEditInitializationRoutineStub($tableName, $columnName, $columnInfo)
    {
        return $this->getCreateInitializationRoutineStub($tableName, $columnName, $columnInfo);
    }

    public function getEditInputProcessorsStub($tableName, $columnName, $columnInfo)
    {
        return '';
    }

    public function getEditPostDbRoutineStub($tableName, $columnName, $columnInfo)
    {
        return '';
    }

    public function getEditRollbackRoutineStub($tableName, $columnName, $columnInfo)
    {
        return $this->getCreateRollbackRoutineStub($tableName, $columnName, $columnInfo);
    }

    public function getDeleteDataProcessingStub($tableName, $columnName, $columnInfo)
    {
        return '';
    }

    public function getDeletePostDbRoutineStub($tableName, $columnName, $columnInfo)
    {
        return '';
    }

    public function getAdditionalMethodsStub($tableName, $columnName, $columnInfo)
    {
        return '';
    }
}
