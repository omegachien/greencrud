<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\DropZone\ColumnFormGenerator;

use GreenPlate\Crud\CrudCreator\ViewGenerator\BaseColumnFormGenerator;

class ColumnFormGenerator extends BaseColumnFormGenerator
{
    public function getFormInputStub($tableName, $columnName, $columnInfo)
    {
        $stubPath = __DIR__ . '/input-text.stub';

        $inputTextStub = file_get_contents($stubPath);

        $fieldHumanReadableName = ucfirst(str_replace('_', ' ', $columnName));
        $fieldSingularCamelName = camel_case(str_singular($columnName));

        $formInputStub = str_replace('{{~fieldHumanReadableName~}}', $fieldHumanReadableName, $inputTextStub);
        $formInputStub = str_replace('{{~fieldName~}}', $columnName, $formInputStub);
        $formInputStub = str_replace('{{~fieldSingularCamelName~}}', $fieldSingularCamelName, $formInputStub);

        return $formInputStub;
    }
}
