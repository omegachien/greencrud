<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\Text;

use GreenPlate\Crud\CrudCreator\MigrationExtractor\BaseMigrationInfo;

class MigrationInfo extends BaseMigrationInfo
{
    /**
     * Adds text type to the migration info
     *
     * @param array $migrationInfo
     * @param array $arguments
     * @return array
     */
    public function addMigrationInfo($migrationInfo, ...$arguments)
    {
        list($fieldName) = $arguments;

        $migrationInfo[$fieldName] = [
            'type' => 'text',
        ];

        return $migrationInfo;
    }
}
