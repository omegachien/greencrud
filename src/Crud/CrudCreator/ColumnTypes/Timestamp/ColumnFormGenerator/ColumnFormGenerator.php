<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\Timestamp\ColumnFormGenerator;

use GreenPlate\Crud\CrudCreator\ColumnTypes\DateTime\ColumnFormGenerator\ColumnFormGenerator as DateTimeColumnFormGenerator;

class ColumnFormGenerator extends DateTimeColumnFormGenerator {}
