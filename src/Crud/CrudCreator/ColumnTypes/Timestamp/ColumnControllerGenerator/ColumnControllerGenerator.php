<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\Timestamp\ColumnControllerGenerator;

use GreenPlate\Crud\CrudCreator\ColumnTypes\DateTime\ColumnControllerGenerator\ColumnControllerGenerator as DateTimeColumnControllerGenerator;

class ColumnControllerGenerator extends DateTimeColumnControllerGenerator {}
