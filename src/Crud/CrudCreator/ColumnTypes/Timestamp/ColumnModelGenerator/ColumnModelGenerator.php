<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\Timestamp\ColumnModelGenerator;

use GreenPlate\Crud\CrudCreator\ColumnTypes\DateTime\ColumnModelGenerator\ColumnModelGenerator as DateTimeColumnModelGenerator;

class ColumnModelGenerator extends DateTimeColumnModelGenerator
{
    public function getNamespaceUses()
    {
        return [
            'Carbon\Carbon',
        ];
    }
}
