<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\Timestamp\ColumnRouteGenerator;

use GreenPlate\Crud\CrudCreator\ColumnTypes\DateTime\ColumnRouteGenerator\ColumnRouteGenerator as DateTimeColumnRouteGenerator;

class ColumnRouteGenerator extends DateTimeColumnRouteGenerator {}
