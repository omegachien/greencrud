<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\MediumText\ColumnControllerGenerator;

use GreenPlate\Crud\CrudCreator\ColumnTypes\Text\ColumnControllerGenerator\ColumnControllerGenerator as TextColumnControllerGenerator;

class ColumnControllerGenerator extends TextColumnControllerGenerator {}