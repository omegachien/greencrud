<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\MediumText\ColumnFormGenerator;

use GreenPlate\Crud\CrudCreator\ColumnTypes\Text\ColumnFormGenerator\ColumnFormGenerator as TextColumnFormGenerator;

class ColumnFormGenerator extends TextColumnFormGenerator {}