<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\MediumText\ColumnRouteGenerator;

use GreenPlate\Crud\CrudCreator\ColumnTypes\Text\ColumnRouteGenerator\ColumnRouteGenerator as TextColumnRouteGenerator;

class ColumnRouteGenerator extends TextColumnRouteGenerator {}
