<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\MediumText\ColumnModelGenerator;

use GreenPlate\Crud\CrudCreator\ColumnTypes\Text\ColumnModelGenerator\ColumnModelGenerator as TextColumnModelGenerator;

class ColumnModelGenerator extends TextColumnModelGenerator {}
