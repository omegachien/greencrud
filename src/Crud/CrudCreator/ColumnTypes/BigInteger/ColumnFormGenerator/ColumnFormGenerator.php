<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\BigInteger\ColumnFormGenerator;

use GreenPlate\Crud\CrudCreator\ColumnTypes\Integer\ColumnFormGenerator\ColumnFormGenerator as IntegerColumnFormGenerator;

class ColumnFormGenerator extends IntegerColumnFormGenerator {}
