<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\BigInteger\ColumnRouteGenerator;

use GreenPlate\Crud\CrudCreator\ColumnTypes\Integer\ColumnRouteGenerator\ColumnRouteGenerator as IntegerColumnRouteGenerator;

class ColumnRouteGenerator extends IntegerColumnRouteGenerator {}
