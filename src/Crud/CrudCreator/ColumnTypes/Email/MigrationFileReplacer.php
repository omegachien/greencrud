<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\Email;

use GreenPlate\Crud\CrudCreator\MigrationGenerator\BaseMigrationFileReplacer;

class MigrationFileReplacer extends BaseMigrationFileReplacer
{
    public function replace($fileContents)
    {
        return str_replace('->email', '->string', $fileContents);
    }
}