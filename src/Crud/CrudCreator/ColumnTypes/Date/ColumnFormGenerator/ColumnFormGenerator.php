<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\Date\ColumnFormGenerator;

use GreenPlate\Crud\CrudCreator\ViewGenerator\BaseColumnFormGenerator;

class ColumnFormGenerator extends BaseColumnFormGenerator
{
    public function getFormInputStub($tableName, $columnName, $columnInfo)
    {
        $stubPath = __DIR__ . '/input-date.stub';

        $inputTextStub = file_get_contents($stubPath);

        $columnHumanReadableName = ucfirst(str_replace('_', ' ', $columnName));
        $columnSingularCamelName = camel_case(str_singular($columnName));

        $formInputStub = str_replace('{{~columnHumanReadableName~}}', $columnHumanReadableName, $inputTextStub);
        $formInputStub = str_replace('{{~columnName~}}', $columnName, $formInputStub);
        $formInputStub = str_replace('{{~columnSingularCamelName~}}', $columnSingularCamelName, $formInputStub);

        return $formInputStub;
    }

    public function getFormJavascriptsStub($tableName, $columnName, $columnInfo)
    {
        $stubPath = __DIR__ . '/javascript-date.stub';

        $javascriptDateStub = file_get_contents($stubPath);

        $javascriptDateStub = str_replace(
            '{{~columnName~}}',
            $columnName,
            $javascriptDateStub
        );

        return $javascriptDateStub;
    }
}
