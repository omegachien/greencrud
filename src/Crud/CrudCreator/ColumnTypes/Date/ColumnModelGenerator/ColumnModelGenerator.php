<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\Date\ColumnModelGenerator;

use GreenPlate\Crud\CrudCreator\ModelGenerator\BaseColumnModelGenerator;

class ColumnModelGenerator extends BaseColumnModelGenerator
{
    public function getNamespaceUses()
    {
        return [
            'Carbon\Carbon',
        ];
    }

    public function getMutatorStub($tableName, $columnName, $columnInfo)
    {
        $stubPath = __DIR__ . '/mutator-date.stub';

        $mutatorDateStub = file_get_contents($stubPath);

        $columnSingularCamelName = str_singular(camel_case($columnName));
        $columnSingularPascalName = ucfirst($columnSingularCamelName);

        $mutatorDateStub = str_replace(
            '{{~columnSingularPascalName~}}',
            $columnSingularPascalName,
            $mutatorDateStub
        );
        $mutatorDateStub = str_replace(
            '{{~columnSingularCamelName~}}',
            $columnSingularCamelName,
            $mutatorDateStub
        );
        $mutatorDateStub = str_replace(
            '{{~columnName~}}',
            $columnName,
            $mutatorDateStub
        );

        return $mutatorDateStub;
    }

    public function getAccessorStub($tableName, $columnName, $columnInfo)
    {
        $stubPath = __DIR__ . '/accessor-date.stub';

        $accessorDateStub = file_get_contents($stubPath);

        $columnSingularCamelName = str_singular(camel_case($columnName));
        $columnSingularPascalName = ucfirst($columnSingularCamelName);

        $accessorDateStub = str_replace(
            '{{~columnSingularPascalName~}}',
            $columnSingularPascalName,
            $accessorDateStub
        );
        $accessorDateStub = str_replace(
            '{{~columnSingularCamelName~}}',
            $columnSingularCamelName,
            $accessorDateStub
        );
        $accessorDateStub = str_replace(
            '{{~columnName~}}',
            $columnName,
            $accessorDateStub
        );

        return $accessorDateStub;
    }
}
