<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\Boolean\ColumnControllerGenerator;

use GreenPlate\Crud\CrudCreator\ControllerGenerator\BaseColumnControllerGenerator;

class ColumnControllerGenerator extends BaseColumnControllerGenerator {
    public function getCreateInputProcessorsStub($tableName, $columnName, $columnInfo)
    {
        $stubPath = __DIR__ . '/processor-boolean.stub';

        $processorBooleanStub = file_get_contents($stubPath);

        $inputProcessorStub = str_replace(
            '{{~columnName~}}',
            $columnName,
            $processorBooleanStub
        );

        return $inputProcessorStub;
    }
}
