<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\Boolean\ColumnFormGenerator;

use GreenPlate\Crud\CrudCreator\ViewGenerator\BaseColumnFormGenerator;

class ColumnFormGenerator extends BaseColumnFormGenerator
{
    public function getFormInputStub($tableName, $columnName, $columnInfo)
    {
        $stubPath = __DIR__ . '/input-boolean.stub';

        $inputBooleanStub = file_get_contents($stubPath);

        $columnHumanReadableName = ucfirst(str_replace('_', ' ', $columnName));
        $columnSingularCamelName = camel_case(str_singular($columnName));

        $formInputStub = str_replace(
            '{{~columnHumanReadableName~}}',
            $columnHumanReadableName,
            $inputBooleanStub)
        ;
        $formInputStub = str_replace(
            '{{~columnName~}}',
            $columnName,
            $formInputStub
        );
        $formInputStub = str_replace(
            '{{~columnSingularCamelName~}}',
            $columnSingularCamelName,
            $formInputStub
        );

        return $formInputStub;
    }
}
