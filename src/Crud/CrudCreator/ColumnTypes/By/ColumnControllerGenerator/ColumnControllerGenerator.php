<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\By\ColumnControllerGenerator;

use GreenPlate\Crud\CrudCreator\ControllerGenerator\BaseColumnControllerGenerator;

class ColumnControllerGenerator extends BaseColumnControllerGenerator
{
    public function getFillableValidation($tableName, $columnName, $columnInfo)
    {
        return [
            'created_' . $columnName => 'required',
            'updated_' . $columnName => 'required',
        ];
    }
}
