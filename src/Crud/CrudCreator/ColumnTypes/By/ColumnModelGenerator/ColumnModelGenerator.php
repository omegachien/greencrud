<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\By\ColumnModelGenerator;

use GreenPlate\Crud\CrudCreator\ModelGenerator\BaseColumnModelGenerator;

class ColumnModelGenerator extends BaseColumnModelGenerator
{
    public function getFillables($tableName, $columnName, $columnInfo)
    {
        return [
            'created_' . $columnName,
            'updated_' . $columnName,
        ];
    }

    public function getBootStub($tableName, $columnName, $columnInfo)
    {
        $stubPath = __DIR__ . '/boot.stub';

        $bootStub = file_get_contents($stubPath);

//        $columnSingularCamelName = str_singular(camel_case($columnName));
//        $columnSingularPascalName = ucfirst($columnSingularCamelName);

//        $bootStub = str_replace(
//            '{{~columnSingularPascalName~}}',
//            $columnSingularPascalName,
//            $bootStub
//        );
//        $bootStub = str_replace(
//            '{{~columnSingularCamelName~}}',
//            $columnSingularCamelName,
//            $bootStub
//        );
//        $bootStub = str_replace(
//            '{{~columnName~}}',
//            $columnName,
//            $bootStub
//        );

        return $bootStub;
    }
}
