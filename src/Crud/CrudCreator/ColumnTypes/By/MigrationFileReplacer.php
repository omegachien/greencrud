<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\By;

use GreenPlate\Crud\CrudCreator\MigrationGenerator\BaseMigrationFileReplacer;

class MigrationFileReplacer extends BaseMigrationFileReplacer
{
    /**
     * replace all location and change to field have extra lat long field
     * @param $fileContents
     * @return mixed
     */
    public function replace($fileContents)
    {
        $byMatches = [];
        preg_match_all('/^.*\b->by\b.*$/m', "'$fileContents'", $byMatches);

        foreach ($byMatches[0] as $match) {
            $fieldNameMatch = [];
            preg_match("/->by\(([^()]+)\)/", $match, $fieldNameMatch);

            $fieldName = trim(trim($fieldNameMatch[1], "'"), '"');
            $quoteChar = substr($fieldNameMatch[1], 0, 1);

            $createdBy = 'created_' . $fieldName;
            $createdByFieldName = $quoteChar . $createdBy . $quoteChar;
            $updatedByFieldName = $quoteChar . 'updated_' . $fieldName . $quoteChar;

            $createdByRow = "\t\t\t" . '$table->integer(' . $createdByFieldName . ')->unsigned()->nullable();';
            $updatedByRow = "\t\t\t" . '$table->integer(' . $updatedByFieldName . ')->unsigned()->nullable();';

            $createdByForeign = "\t\t\t" . '$table->foreign(' . $createdByFieldName . ')->references(\'id\')->on(\'users\');';
            $updatedByForeign = "\t\t\t" . '$table->foreign(' . $updatedByFieldName . ')->references(\'id\')->on(\'users\');';

            $byFields = $createdByRow . PHP_EOL . $updatedByRow . PHP_EOL . $createdByForeign . PHP_EOL . $updatedByForeign;

            $fileContents = str_replace($match, $byFields, $fileContents);
        }

        return $fileContents;
    }
}