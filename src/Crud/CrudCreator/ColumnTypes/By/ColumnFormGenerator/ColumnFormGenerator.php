<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\By\ColumnFormGenerator;

use GreenPlate\Crud\CrudCreator\ViewGenerator\BaseColumnFormGenerator;

class ColumnFormGenerator extends BaseColumnFormGenerator
{
    public function getOldValueStub($tableName, $columnName, $columnInfo)
    {
        return '';
    }

    public function getFormInputStub($tableName, $columnName, $columnInfo)
    {
        $stubPath = __DIR__ . '/input-by.stub';

        $inputLocationStub = file_get_contents($stubPath);

        $fieldSingularCamelName = camel_case(str_singular($columnName));
        $fieldHumanReadableName = ucfirst(str_replace('_', ' ', $columnName));

        $formInputStub = str_replace('{{~entityCamelName~}}', camel_case($tableName), $inputLocationStub);
        $formInputStub = str_replace('{{~fieldHumanReadableName~}}', $fieldHumanReadableName, $formInputStub);
        $formInputStub = str_replace('{{~fieldName~}}', $columnName, $formInputStub);
        $formInputStub = str_replace('{{~fieldSingularCamelName~}}', $fieldSingularCamelName, $formInputStub);

        return $formInputStub;
    }
}
