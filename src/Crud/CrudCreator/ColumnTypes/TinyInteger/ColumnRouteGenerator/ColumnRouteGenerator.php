<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\TinyInteger\ColumnRouteGenerator;

use GreenPlate\Crud\CrudCreator\ColumnTypes\Integer\ColumnRouteGenerator\ColumnRouteGenerator as IntegerColumnRouteGenerator;

class ColumnRouteGenerator extends IntegerColumnRouteGenerator {}
