<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\Char\ColumnControllerGenerator;

use GreenPlate\Crud\CrudCreator\ColumnTypes\String\ColumnControllerGenerator\ColumnControllerGenerator as StringColumnControllerGenerator;

class ColumnControllerGenerator extends StringColumnControllerGenerator {}
