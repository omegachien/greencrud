<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\Char\ColumnFormGenerator;

use GreenPlate\Crud\CrudCreator\ColumnTypes\String\ColumnFormGenerator\ColumnFormGenerator as StringColumnFormGenerator;

class ColumnFormGenerator extends StringColumnFormGenerator {}
