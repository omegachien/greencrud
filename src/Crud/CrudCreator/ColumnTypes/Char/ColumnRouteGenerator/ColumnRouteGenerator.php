<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\Char\ColumnRouteGenerator;

use GreenPlate\Crud\CrudCreator\ColumnTypes\String\ColumnRouteGenerator\ColumnRouteGenerator as StringColumnRouteGenerator;

class ColumnRouteGenerator extends StringColumnRouteGenerator {}
