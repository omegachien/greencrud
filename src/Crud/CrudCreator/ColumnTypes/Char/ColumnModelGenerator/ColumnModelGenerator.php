<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\Char\ColumnModelGenerator;

use GreenPlate\Crud\CrudCreator\ColumnTypes\String\ColumnModelGenerator\ColumnModelGenerator as StringColumnModelGenerator;

class ColumnModelGenerator extends StringColumnModelGenerator {}
