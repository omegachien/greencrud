<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\Tinymce\ColumnModelGenerator;

use GreenPlate\Crud\CrudCreator\ModelGenerator\BaseColumnModelGenerator;

class ColumnModelGenerator extends BaseColumnModelGenerator
{
    public function getMutatorStub($tableName, $columnName, $columnInfo)
    {
        $stubPath = __DIR__ . '/mutator-tinymce.stub';

        $mutatorTinymceStub = file_get_contents($stubPath);

        $columnSingularCamelName = str_singular(camel_case($columnName));
        $columnSingularPascalName = ucfirst($columnSingularCamelName);

        $mutatorTinymceStub = str_replace(
            '{{~columnSingularPascalName~}}',
            $columnSingularPascalName,
            $mutatorTinymceStub
        );
        $mutatorTinymceStub = str_replace(
            '{{~columnSingularCamelName~}}',
            $columnSingularCamelName,
            $mutatorTinymceStub
        );
        $mutatorTinymceStub = str_replace(
            '{{~columnName~}}',
            $columnName,
            $mutatorTinymceStub
        );

        return $mutatorTinymceStub;
    }

    public function getNamespaceUses()
    {
        return [
            'HTMLPurifier',
            'HTMLPurifier_Config',
        ];
    }
}
