<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\Tinymce\ColumnFormGenerator;

use GreenPlate\Crud\CrudCreator\ViewGenerator\BaseColumnFormGenerator;

class ColumnFormGenerator extends BaseColumnFormGenerator
{
    public function getFormInputStub($tableName, $columnName, $columnInfo)
    {
        $stubPath = __DIR__ . '/input-textarea.stub';

        $inputTextAreaStub = file_get_contents($stubPath);

        $columnHumanReadableName = ucfirst(str_replace('_', ' ', $columnName));
        $columnSingularCamelName = camel_case(str_singular($columnName));

        $formInputStub = str_replace(
            '{{~columnHumanReadableName~}}',
            $columnHumanReadableName,
            $inputTextAreaStub
        );
        $formInputStub = str_replace(
            '{{~columnName~}}',
            $columnName,
            $formInputStub
        );
        $formInputStub = str_replace(
            '{{~columnSingularCamelName~}}',
            $columnSingularCamelName,
            $formInputStub
        );

        return $formInputStub;
    }

    public function getFormJavascriptsIncludeStub($tableName, $columnName, $columnInfo)
    {
        return
            '<script src="{{url(\'vendor/tinymce/tinymce.min.js\')}}"></script>';
    }

    public function getFormJavascriptsStub($tableName, $columnName, $columnInfo)
    {
        $stubPath = __DIR__ . '/javascript-tinymce.stub';

        $javascriptTinymceStub = file_get_contents($stubPath);

        $javascriptTinymceStub = str_replace(
            '{{~columnName~}}',
            $columnName,
            $javascriptTinymceStub
        );

        return $javascriptTinymceStub;
    }
}
