<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\Tinymce;

use GreenPlate\Crud\CrudCreator\MigrationGenerator\BaseMigrationFileReplacer;

class MigrationFileReplacer extends BaseMigrationFileReplacer
{
    public function replace($fileContents)
    {
        return str_replace('->tinymce', '->text', $fileContents);
    }
}