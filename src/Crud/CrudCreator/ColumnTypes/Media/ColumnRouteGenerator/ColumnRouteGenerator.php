<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\Media\ColumnRouteGenerator;

use GreenPlate\Crud\CrudCreator\RouteGenerator\BaseColumnRouteGenerator;

class ColumnRouteGenerator extends BaseColumnRouteGenerator
{

    public function getAdditionalRoutesStub($fieldName)
    {
        return 'Route::post(\'{{~entityRoutePrefix~}}/a/' . $fieldName . '/upload\', \'{{~entityNamespaceName~}}\Backend\{{~contollerClassName~}}@ajax' . ucfirst($fieldName) . 'Upload\');' . PHP_EOL .
            'Route::post(\'{{~entityRoutePrefix~}}/a/' . $fieldName . '/delete\', \'{{~entityNamespaceName~}}\Backend\{{~contollerClassName~}}@ajax' . ucfirst($fieldName) . 'Delete\');' . PHP_EOL .
            'Route::post(\'{{~entityRoutePrefix~}}/a/' . $fieldName . '/sequence\', \'{{~entityNamespaceName~}}\Backend\{{~contollerClassName~}}@ajax' . ucfirst($fieldName) . 'Sequence\');' . PHP_EOL .
            'Route::post(\'{{~entityRoutePrefix~}}/a/' . $fieldName . '/caption\', \'{{~entityNamespaceName~}}\Backend\{{~contollerClassName~}}@ajax' . ucfirst($fieldName) . 'Caption\');';
    }

}
