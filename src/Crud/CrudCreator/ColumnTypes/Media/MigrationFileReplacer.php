<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\Media;

use GreenPlate\Crud\CrudCreator\MigrationGenerator\BaseMigrationFileReplacer;

class MigrationFileReplacer extends BaseMigrationFileReplacer
{
    public function replace($fileContents)
    {
        //Remove the line
        $rows = explode("\n", $fileContents);
        $unwanted = "->media";
        $cleanArray = preg_grep("/$unwanted/i", $rows, PREG_GREP_INVERT);
        $cleanString = implode("\n", $cleanArray);

        return $cleanString;
    }
}