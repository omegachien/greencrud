<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\Media\ColumnModelGenerator;

use GreenPlate\Crud\CrudCreator\ModelGenerator\BaseColumnModelGenerator;

class ColumnModelGenerator extends BaseColumnModelGenerator
{
    public function getNamespaceUses()
    {
        return [
            'Spatie\MediaLibrary\HasMedia\HasMediaTrait',
            'Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;'
        ];
    }

    public function getInterface()
    {
        return ['HasMediaConversions'];
    }

    public function getFillables($tableName, $columnName, $columnInfo)
    {
        return [];
    }

    public function getTraitUses()
    {
        return ['HasMediaTrait'];
    }

    public function getHelperStub($tableName, $columnName, $columnInfo)
    {
        $stubPath = __DIR__ . '/thumb.stub';

        $thumbMethod = file_get_contents($stubPath);

        return $thumbMethod;
    }
}
