<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\Media\ColumnControllerGenerator;

use GreenPlate\Crud\CrudCreator\ControllerGenerator\BaseColumnControllerGenerator;
use GreenPlate\Helper\Helper;

class ColumnControllerGenerator extends BaseColumnControllerGenerator
{
    public function getNamespaceUses()
    {
        return [];
    }

    public function getFillableValidation($tableName, $columnName, $columnInfo)
    {
        return [];
    }

    public function getEditInputProcessorsStub($tableName, $columnName, $columnInfo)
    {
//        $stubPath = __DIR__ . '/processor-image-edit.stub';
//
//        $processorInputEditImageStub = file_get_contents($stubPath);
//
//        $entityCamelName = camel_case(str_singular($tableName));
//        $columnPascalName = ucfirst(camel_case($columnName));
//        $modelClass = studly_case(str_singular($tableName));
//        $modelClassColumnSpinalCaseName = Helper::spinalCase(
//            $modelClass . '-' . $columnName
//        );
//
//        $processorInputEditImageStub = str_replace(
//            '{{~columnPascalName~}}',
//            $columnPascalName,
//            $processorInputEditImageStub
//        );
//        $processorInputEditImageStub = str_replace(
//            '{{~entityCamelName~}}',
//            $entityCamelName,
//            $processorInputEditImageStub
//        );
//        $processorInputEditImageStub = str_replace(
//            '{{~columnName~}}',
//            $columnName,
//            $processorInputEditImageStub
//        );
//        $processorInputEditImageStub = str_replace(
//            '{{~modelClassColumnSpinalCaseName~}}',
//            $modelClassColumnSpinalCaseName,
//            $processorInputEditImageStub
//        );
//
//        return $processorInputEditImageStub;
    }

//    public function getDeleteDataProcessingStub($tableName, $columnName, $columnInfo)
//    {
//        $stubPath = __DIR__ . '/processor-image-delete.stub';
//
//        $processorInputDeleteImageStub = file_get_contents($stubPath);
//
//        $entityCamelName = camel_case(str_singular($tableName));
//        $columnPascalName = ucfirst(camel_case($columnName));
//
//        $processorInputDeleteImageStub = str_replace(
//            '{{~columnPascalName~}}',
//            $columnPascalName,
//            $processorInputDeleteImageStub
//        );
//        $processorInputDeleteImageStub = str_replace(
//            '{{~entityCamelName~}}',
//            $entityCamelName,
//            $processorInputDeleteImageStub
//        );
//        $processorInputDeleteImageStub = str_replace(
//            '{{~columnName~}}',
//            $columnName,
//            $processorInputDeleteImageStub
//        );
//
//        return $processorInputDeleteImageStub;
//    }

//    public function getDeletePostDbRoutineStub($tableName, $columnName, $columnInfo)
//    {
//        $stubPath = __DIR__ . '/post-db-image-edit-delete.stub';
//
//        $postDbImageEditDeleteStub = file_get_contents($stubPath);
//
//        $columnPascalName = ucfirst(camel_case($columnName));
//
//        $imageEditDeletePostDbRoutine = str_replace('{{~columnPascalName~}}',
//            $columnPascalName,
//            $postDbImageEditDeleteStub
//        );
//
//        return $imageEditDeletePostDbRoutine;
//    }

    public function getAdditionalMethodsStub($tableName, $columnName, $columnInfo)
    {
        $stubPath = __DIR__ . '/post-ajax-upload.stub';
        $stubPathDelete = __DIR__ . '/post-ajax-delete.stub';
        $stubPathSequence = __DIR__ . '/post-ajax-sequence.stub';
        $stubPathCaption = __DIR__ . '/post-ajax-caption.stub';

        $postAjaxUploadContent = file_get_contents($stubPath);
        $postAjaxDeleteContent = file_get_contents($stubPathDelete);
        $postAjaxSequenceContent = file_get_contents($stubPathSequence);
        $postAjaxCaptionContent = file_get_contents($stubPathCaption);

        $entityCamelName = camel_case(str_singular($tableName));
        $modelClass = studly_case(str_singular($tableName));

        $imageDelete = '';
        if(isset($columnInfo['multiple']) && ! $columnInfo['multiple']){
            $imageDelete = '${{~entityCamelName~}}->getMedia()->each(function ($image) {
                $custom = $image->getCustomProperty(\'custom\');
                if ($custom == \''. $columnInfo['custom'] .'\') {
                    $image->delete();
                }
            });';
        }

        $customProperty = '';
        $addMedia = '$media = ${{~entityCamelName~}}->addMediaFromRequest(\'image\')->toMediaCollection();';
        if(isset($columnInfo['custom']) && ! empty($columnInfo['custom'])){
            $addMedia = '$media = ${{~entityCamelName~}}->addMediaFromRequest(\'image\')
                                        ->withCustomProperties([\'custom\' => \''. $columnInfo['custom'] .'\'])
                                        ->toMediaCollection();';
            $customProperty = ucfirst($columnInfo['custom']);
        }

        $postAjaxUploadContent = str_replace('{{~customProperty~}}', $customProperty, $postAjaxUploadContent);
        $postAjaxUploadContent = str_replace('{{~imageDelete~}}', $imageDelete, $postAjaxUploadContent);
        $postAjaxUploadContent = str_replace('{{~addMedia~}}', $addMedia, $postAjaxUploadContent);
        $postAjaxUploadContent = str_replace('{{~entityCamelName~}}', $entityCamelName, $postAjaxUploadContent);
        $postAjaxUploadContent = str_replace('{{~$modelClass~}}', $modelClass, $postAjaxUploadContent);

        $postAjaxDeleteContent = str_replace('{{~customProperty~}}', $customProperty, $postAjaxDeleteContent);
        $postAjaxDeleteContent = str_replace('{{~entityCamelName~}}', $entityCamelName, $postAjaxDeleteContent);
        $postAjaxDeleteContent = str_replace('{{~$modelClass~}}', $modelClass, $postAjaxDeleteContent);

        $postAjaxSequenceContent = str_replace('{{~customProperty~}}', $customProperty, $postAjaxSequenceContent);
        $postAjaxCaptionContent = str_replace('{{~customProperty~}}', $customProperty, $postAjaxCaptionContent);

        return $postAjaxUploadContent . PHP_EOL . PHP_EOL . $postAjaxDeleteContent . PHP_EOL . $postAjaxSequenceContent . PHP_EOL . $postAjaxCaptionContent;
    }
}
