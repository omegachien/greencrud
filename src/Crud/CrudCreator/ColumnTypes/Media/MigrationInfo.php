<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\Media;

use GreenPlate\Crud\CrudCreator\MigrationExtractor\BaseMigrationInfo;

class MigrationInfo extends BaseMigrationInfo
{
    /**
     * Adds number type to the migration info
     *
     * @param array $migrationInfo
     * @param array $arguments
     * @return array
     */
    public function addMigrationInfo($migrationInfo, ...$arguments)
    {
        $multiple = false;
        list($fieldName) = $arguments;

        if(count($arguments) == 2) {
            $customize = $arguments[1];
            if (isset($customize['multiple']) && $customize['multiple']) {
                $multiple = true;
            }
        }

        $migrationInfo[$fieldName] = [
            'type'     => 'media',
            'multiple' => $multiple,
            'custom' => $fieldName,
            'options'  => [
                'isFile' => true,
            ]
        ];

        return $migrationInfo;
    }
}
