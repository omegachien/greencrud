<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\Media\ColumnFormGenerator;

use GreenPlate\Crud\CrudCreator\ViewGenerator\BaseColumnFormGenerator;

class ColumnFormGenerator extends BaseColumnFormGenerator
{
//    public function getOldValueStub($tableName, $columnName, $columnInfo)
//    {
//        $stubPath = __DIR__ . '/old-value-image.stub';
//
//        $oldValueImageStub = file_get_contents($stubPath);
//
//        $columnSingularCamelName = camel_case(str_singular($columnName));
//        $entityCamelName = camel_case(str_singular($tableName));
//
//        $oldValueImageStub = str_replace(
//            '{{~columnSingularCamelName~}}',
//            $columnSingularCamelName,
//            $oldValueImageStub
//        );
//        $oldValueImageStub = str_replace(
//            '{{~entityCamelName~}}',
//            $entityCamelName,
//            $oldValueImageStub
//        );
//        $oldValueImageStub = str_replace(
//            '{{~columnName~}}',
//            $columnName,
//            $oldValueImageStub
//        );
//
//        return $oldValueImageStub;
//    }

    public function getAfterPartialForm($tableName, $columnName, $columnInfo)
    {
        $stubPath = __DIR__ . '/input-image.stub';
        $inputImageStub = file_get_contents($stubPath);

        $inputImageStub = str_replace('{{~customProperty~}}', $columnName, $inputImageStub);

        $message = '(Maximum 1 image)';
        if(isset($columnInfo['multiple']) && $columnInfo['multiple']){
            $message = '';
        }
        $inputImageStub = str_replace('{{~labelMultiple~}}', $message, $inputImageStub);

        return $inputImageStub;
    }

    public function getExtraJavascript($tableName, $columnName, $columnInfo)
    {
        $stubPath = __DIR__ . '/file-uploader.stub';

        $extraJsContent = file_get_contents($stubPath);

        $entityCamelName = camel_case(str_singular($tableName));
        $modelClass = studly_case(str_singular($tableName));

        $extraJsContent = str_replace('{{~customPropertyUpper~}}', ucfirst($columnName), $extraJsContent);
        $extraJsContent = str_replace('{{~entityCamelName~}}', $entityCamelName, $extraJsContent);
        $extraJsContent = str_replace('{{~$modelClass~}}', $modelClass, $extraJsContent);

        $multipleValue = 'false';
        $removeImage = '$("#uploaded_{{~customProperty~}}_photos").empty()';
        if(isset($columnInfo['multiple']) && $columnInfo['multiple']){
            $multipleValue = 'true';
            $removeImage = '';
        }

        $extraJsContent = str_replace(
            '{{~removeImage~}}',
            $removeImage,
            $extraJsContent
        );

        $extraJsContent = str_replace(
            '{{~multiple~}}',
            $multipleValue,
            $extraJsContent
        );
        $extraJsContent = str_replace('{{~customProperty~}}', $columnName, $extraJsContent);

        return $extraJsContent;
    }

    public function getCustomCss($tableName, $columnName, $columnInfo)
    {
        $stubPath = __DIR__ . '/custom-css.stub';

        $customCssContent = file_get_contents($stubPath);

        return $customCssContent;
    }

//    public function getFormInputStub($tableName, $columnName, $columnInfo)
//    {
//        $stubPath = __DIR__ . '/input-image.stub';
//
//        $inputImageStub = file_get_contents($stubPath);
//
//        $columnHumanReadableName
//            = ucfirst(str_replace('_', ' ', $columnName));
//        $columnSingularCamelName = camel_case(str_singular($columnName));
//
//        $inputImageStub = str_replace(
//            '{{~columnHumanReadableName~}}',
//            $columnHumanReadableName,
//            $inputImageStub
//        );
//        $inputImageStub = str_replace(
//            '{{~columnSingularCamelName~}}',
//            $columnSingularCamelName,
//            $inputImageStub
//        );
//        $inputImageStub = str_replace(
//            '{{~columnName~}}',
//            $columnName,
//            $inputImageStub
//        );
//
//        return $inputImageStub;
//    }
}
