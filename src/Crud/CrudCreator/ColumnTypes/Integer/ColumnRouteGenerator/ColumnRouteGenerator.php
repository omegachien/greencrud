<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\Integer\ColumnRouteGenerator;

use GreenPlate\Crud\CrudCreator\RouteGenerator\BaseColumnRouteGenerator;

class ColumnRouteGenerator extends BaseColumnRouteGenerator
{
    public function getAdditionalRoutesStub($fieldName)
    {
        return '';
    }
}
