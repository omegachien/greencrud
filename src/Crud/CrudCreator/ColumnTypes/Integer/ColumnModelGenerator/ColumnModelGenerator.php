<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\Integer\ColumnModelGenerator;

use GreenPlate\Crud\CrudCreator\ModelGenerator\BaseColumnModelGenerator;

class ColumnModelGenerator extends BaseColumnModelGenerator
{
    public function getNamespaceUses()
    {
        return [];
    }

    public function getTraitUses()
    {
        return [];
    }

    public function getHelperStub($tableName, $columnName, $columnInfo)
    {
        return '';
    }

    public function getMutatorStub($tableName, $columnName, $columnInfo)
    {
        return '';
    }

    public function getAccessorStub($tableName, $columnName, $columnInfo)
    {
        return '';
    }
}
