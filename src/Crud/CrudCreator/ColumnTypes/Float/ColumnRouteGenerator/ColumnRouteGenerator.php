<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\Float\ColumnRouteGenerator;

use GreenPlate\Crud\CrudCreator\ColumnTypes\Integer\ColumnRouteGenerator\ColumnRouteGenerator as IntegerColumnRouteGenerator;

class ColumnRouteGenerator extends IntegerColumnRouteGenerator {}
