<?php

namespace GreenPlate\Crud\CrudCreator\ColumnTypes\Float\ColumnControllerGenerator;

use GreenPlate\Crud\CrudCreator\ColumnTypes\Integer\ColumnControllerGenerator\ColumnControllerGenerator as IntegerColumnControllerGenerator;

class ColumnControllerGenerator extends IntegerColumnControllerGenerator {}
