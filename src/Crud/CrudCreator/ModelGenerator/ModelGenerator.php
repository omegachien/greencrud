<?php

namespace GreenPlate\Crud\CrudCreator\ModelGenerator;

use GreenPlate\Crud\CrudHelpers;
use GreenPlate\Helper\Helper;

class ModelGenerator
{
    /**
     * Array storing all the model infos
     *
     * @var array
     */
    protected $modelInfos;

    /**
     * String directory path
     *
     * @var string
     */
    protected $stubDirectory;

    /**
     * An array storing all the ColumnModelGenerators required
     *
     * @var array
     */
    protected $columnModelGenerators = [];

    /**
     * Initialize ModelGenerator
     *
     * @param array $modelInfos
     */
    public function __construct($modelInfos)
    {
        $this->modelInfos = $modelInfos;
        $this->stubDirectory = __DIR__ . '/stubs';

        $this->columnModelGenerators =
            CrudHelpers::initializeColumnModelGenerator($modelInfos);

    }

    public function generate()
    {
        $modelDirectory = base_path('app/Models');

        foreach ($this->modelInfos as $modelInfo) {
            // start with base
            $modelFileContent =
                file_get_contents($this->stubDirectory . '/base-model.stub');

            // Generating {{~modelClass~}}
            $modelFileContent = str_replace(
                '{{~modelClass~}}',
                $modelInfo->getModelClass(),
                $modelFileContent
            );

            // Generating {{~tableName~}}
            $modelFileContent = str_replace(
                '{{~tableName~}}',
                $modelInfo->getTableName(),
                $modelFileContent
            );

            // Generating {{~namespaceUses~}}
            $modelFileContent = $this->generateNamespaceUses($modelFileContent, $modelInfo);

            // Generating {{~implement~}}
            $modelFileContent = $this->generateImplement($modelFileContent, $modelInfo);

            // Generating {{~traitUses~}}
            $modelFileContent = $this->generateTraitUses($modelFileContent, $modelInfo);

            // Generating {{~fillable~}}
            $modelFileContent = $this->generateFillable($modelFileContent, $modelInfo);

            // Generating {{~timestamps~}}
            $modelFileContent = $this->generateTimestamps($modelFileContent, $modelInfo);

            // Generating {{~boot~}}
            $modelFileContent = $this->generateBoot($modelFileContent, $modelInfo);

            // Generating {{~helpers~}}
            $modelFileContent = $this->generateHelpers($modelFileContent, $modelInfo);

            // Generating {{~mutators~}}
            $modelFileContent = $this->generateMutators($modelFileContent, $modelInfo);

            // Generating {{~accessors~}}
            $modelFileContent = $this->generateAccessors($modelFileContent, $modelInfo);

            // Generating {{~tableRelationship~}}
            $modelFileContent = $this->generateTableRelationship($modelFileContent, $modelInfo);

            if ( ! file_exists('app/Models')) {
                mkdir('app/Models');
            }

            file_put_contents(
                base_path('app/Models') . '/' . $modelInfo->getModelClass() . '.php',
                $modelFileContent
            );
        }
    }

    private function generateNamespaceUses($modelFileContent, $modelInfo)
    {
        $namespaceUsesArray = [
            'Illuminate\Database\Eloquent\Model'
        ];

        foreach ($modelInfo->getFillableTypes() as $type) {
            $typeNamespaceUses = $this->columnModelGenerators[$type]->getNamespaceUses();
            $namespaceUsesArray = array_merge($namespaceUsesArray, $typeNamespaceUses);
        }
        $namespaceUsesArray = array_unique($namespaceUsesArray);
        sort($namespaceUsesArray);

        $namespaceUsesString = '';
        foreach ($namespaceUsesArray as $namespaceUse) {
            $namespaceUsesString .= 'use ' . $namespaceUse . ';' . PHP_EOL;
        }

        $modelFileContent =
            str_replace('{{~namespaceUses~}}', $namespaceUsesString, $modelFileContent);

        return $modelFileContent;
    }

    private function generateImplement($modelFileContent, $modelInfo)
    {
        $interfaces = [];

        foreach ($modelInfo->getFillableTypes() as $type) {
            $typeNamespaceUses = $this->columnModelGenerators[$type]->getInterface();
            $interfaces = array_merge($interfaces, $typeNamespaceUses);
        }
        $interfaces = array_unique($interfaces);
        sort($interfaces);

        $namespaceUsesString = 'implements ';
        foreach ($interfaces as $namespaceUse) {
            $namespaceUsesString .= $namespaceUse;
        }

        $modelFileContent =
            str_replace('{{~implement~}}', $namespaceUsesString, $modelFileContent);

        return $modelFileContent;
    }

    private function generateTraitUses($modelFileContent, $modelInfo)
    {
        $traitUsesArray = [];

        foreach ($modelInfo->getFillableTypes() as $type) {
            $typeTraitUses = $this->columnModelGenerators[$type]->getTraitUses();
            $traitUsesArray = array_merge($traitUsesArray, $typeTraitUses);
        }
        sort($traitUsesArray);

        $traitUsesString = '';
        foreach ($traitUsesArray as $traitUse) {
            $traitUsesString .= TAB_SPACE . 'use ' . $traitUse . ';' . PHP_EOL;
        }

        $modelFileContent =
            str_replace('{{~traitUses~}}', $traitUsesString, $modelFileContent);

        return $modelFileContent;
    }

    private function generateFillable($modelFileContent, $modelInfo)
    {
        $fillablesArray = [];

        foreach ($modelInfo->getFillables() as $fieldName => $fieldInfo) {
            $type = $fieldInfo['type'];
            $typeFillables =
                $this->columnModelGenerators[$type]->getFillables(
                    $modelInfo->getTableName(),
                    $fieldName,
                    $fieldInfo
                );
            $fillablesArray = array_merge($fillablesArray, $typeFillables);
        }

        // Construct fillable fields
        //
        //   'status',
        //   'name',
        //
        $fillablesString = '';
        foreach ($fillablesArray as $fillable) {
            $fillablesString .=
                str_repeat(TAB_SPACE, 2) . "'" . $fillable . "'," . PHP_EOL;
        }
        $fillablesString = rtrim($fillablesString, PHP_EOL);

        // Insert fillable fields into fillable array
        // protected $fillable = [
        //   'status',
        //   'name',
        // ];
        $fillableStub = file_get_contents($this->stubDirectory . '/fillable.stub');
        $fillablesString =
            str_replace('{{~fillable~}}', $fillablesString, $fillableStub);

        // Replace into model file content
        $modelFileContent =
            str_replace('{{~fillables~}}', $fillablesString, $modelFileContent);

        return $modelFileContent;
    }

    private function generateTimestamps($modelFileContent, $modelInfo)
    {
        $timestampsString = '';
        switch ($modelInfo->getTimestampsType()) {
            case 'timestampsTz':
                $timestampsString = "protected \$dateFormat = 'Y-m-d H:i:sO';";
                break;

            case null:
                $timestampsString =
                    file_get_contents($this->stubDirectory . '/no-timestamps.stub');
                break;

            default:
                break;
        }
        if ( ! empty($timestampsString)) {
            $timestampsString =
                str_repeat(PHP_EOL, 2) .
                Helper::stringLinePrefix(TAB_SPACE, $timestampsString) .
                PHP_EOL;
        }

        // Replace into model file content
        $modelFileContent =
            str_replace('{{~timestamps~}}', $timestampsString, $modelFileContent);

        return $modelFileContent;
    }

    private function generateBoot($modelFileContent, $modelInfo)
    {
        $boots = '';
        $bootStub =
            file_get_contents($this->stubDirectory . '/boot.stub');

        $modelFileContent =
            str_replace('{{~boot~}}', $bootStub, $modelFileContent);

        foreach ($modelInfo->getFillables() as $fieldName => $fieldInfo) {
            $type = $fieldInfo['type'];
            $boots .= PHP_EOL .
                $this->columnModelGenerators[$type]
                    ->getBootStub(
                        $modelInfo->getTableName(),
                        $fieldName,
                        $fieldInfo
                    );
        }
        $boots = rtrim($boots, PHP_EOL);

        $modelFileContent =
            str_replace('{{~boot~}}', $boots, $modelFileContent);

        return $modelFileContent;
    }

    private function generateHelpers($modelFileContent, $modelInfo)
    {
        $helpers = '';

        foreach ($modelInfo->getFillables() as $fieldName => $fieldInfo) {
            $type = $fieldInfo['type'];
            $helperContent = PHP_EOL .
                $this->columnModelGenerators[$type]
                    ->getHelperStub(
                        $modelInfo->getTableName(),
                        $fieldName,
                        $fieldInfo
                    );
            if (strpos($helpers, $helperContent) == false) {
                $helpers = $helpers . $helperContent;
            }
        }
        $helpers = rtrim($helpers, PHP_EOL);

        if ( ! empty($helpers)) {
            $helpers = PHP_EOL . Helper::stringLinePrefix(TAB_SPACE, $helpers);
        }

        $modelFileContent =
            str_replace('{{~helpers~}}', $helpers, $modelFileContent);

        return $modelFileContent;
    }

    private function generateMutators($modelFileContent, $modelInfo)
    {
        $mutators = '';

        foreach ($modelInfo->getFillables() as $fieldName => $fieldInfo) {
            $type = $fieldInfo['type'];
            $mutators .= PHP_EOL .
                $this->columnModelGenerators[$type]->getMutatorStub(
                    $modelInfo->getTableName(),
                    $fieldName,
                    $fieldInfo
                );
        }
        $mutators = rtrim($mutators, PHP_EOL);

        if ( ! empty($mutators)) {
            $mutators = PHP_EOL . Helper::stringLinePrefix(TAB_SPACE, $mutators);
        }

        $modelFileContent =
            str_replace('{{~mutators~}}', $mutators, $modelFileContent);

        return $modelFileContent;
    }

    private function generateAccessors($modelFileContent, $modelInfo)
    {
        $accessors = '';

        foreach ($modelInfo->getFillables() as $fieldName => $fieldInfo) {
            $type = $fieldInfo['type'];
            $accessors .= PHP_EOL .
                $this->columnModelGenerators[$type]->getAccessorStub(
                    $modelInfo->getTableName(),
                    $fieldName,
                    $fieldInfo
                );
        }
        $accessors = rtrim($accessors, PHP_EOL);

        if ( ! empty($accessors)) {
            $accessors = PHP_EOL . Helper::stringLinePrefix(TAB_SPACE, $accessors);
        }

        $modelFileContent =
            str_replace('{{~accessors~}}', $accessors, $modelFileContent);

        return $modelFileContent;
    }

    private function generateTableRelationship($modelFileContent, $modelInfo)
    {
        $tableRelationship = '';

        foreach ($modelInfo->getRelationships() as $relationshipInfo) {
            if ($relationshipInfo['relationship'] != 'junction-table') {
                $tableRelationshipContent = '';

                switch ($relationshipInfo['relationship']) {
                    case 'one-to-many':
                        $tableRelationshipContent = file_get_contents(
                            $this->stubDirectory . '/one-to-many.stub'
                        );

                        $foreignEntityName =
                            camel_case(str_plural(
                                $relationshipInfo['foreign_table']
                            ));
                        break;
                    case 'many-to-one':
                        $tableRelationshipContent = file_get_contents(
                            $this->stubDirectory . '/many-to-one.stub'
                        );

                        $foreignEntityName =
                            camel_case(str_singular(
                                $relationshipInfo['foreign_table']
                            ));
                        break;
                    case 'many-to-many':
                        $tableRelationshipContent = file_get_contents(
                            $this->stubDirectory . '/many-to-many.stub'
                        );

                        $foreignEntityName =
                            camel_case(str_plural(
                                $relationshipInfo['foreign_table']
                            ));
                        break;

                    default:
                        break;
                }

                $foreignModelClass =
                    studly_case(str_singular($relationshipInfo['foreign_table']));

                $tableRelationshipContent =
                    str_replace(
                        '{{~foreignEntityName~}}',
                        $foreignEntityName,
                        $tableRelationshipContent
                    );
                $tableRelationshipContent =
                    str_replace(
                        '{{~foreignModelClass~}}',
                        $foreignModelClass,
                        $tableRelationshipContent
                    );

                $tableRelationship .= str_repeat(PHP_EOL, 2) .
                    Helper::stringLinePrefix(TAB_SPACE, $tableRelationshipContent);
            }
        }

        $modelFileContent =
            str_replace('{{~tableRelationship~}}', $tableRelationship, $modelFileContent);

        return $modelFileContent;
    }
}
