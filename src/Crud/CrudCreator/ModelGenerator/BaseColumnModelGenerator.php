<?php

namespace GreenPlate\Crud\CrudCreator\ModelGenerator;

class BaseColumnModelGenerator
{
    public function getNamespaceUses()
    {
        return [];
    }

    public function getInterface()
    {
        return [];
    }

    public function getTraitUses()
    {
        return [];
    }

    public function getFillables($tableName, $columnName, $columnInfo)
    {
        return [$columnName];
    }

    public function getHelperStub($tableName, $columnName, $columnInfo)
    {
        return '';
    }

    public function getMutatorStub($tableName, $columnName, $columnInfo)
    {
        return '';
    }

    public function getAccessorStub($tableName, $columnName, $columnInfo)
    {
        return '';
    }

    public function getBootStub($tableName, $columnName, $columnInfo)
    {
        return '';
    }
}
