<?php

namespace GreenPlate\Crud\CrudCreator\MigrationGenerator;

abstract class BaseMigrationFileReplacer
{
    abstract public function replace($fileContents);
}