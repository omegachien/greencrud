<?php

namespace GreenPlate\Crud\CrudCreator\MigrationGenerator;

use GreenPlate\Crud\CrudHelpers;
use Illuminate\Filesystem\Filesystem;

class MigrationGenerator
{

    /**
     * The filesystem instance.
     *
     * @var \Illuminate\Filesystem\Filesystem
     */
    protected $files;

    /**
     * Create a new controller creator command instance.
     *
     * @param  \Illuminate\Filesystem\Filesystem  $files
     * @return void
     */
    public function __construct()
    {
        $this->files = new Filesystem();
    }

    public function generate($crudMigrationList, $modelInfos)
    {
        $migrationDirectory = base_path('database/migrations');

        foreach ($crudMigrationList as $migrationClass => $migrationPath) {
            $this->originalFileName = pathinfo($migrationPath, PATHINFO_FILENAME);
            // Convert back from crud migration to normal migration and copy into
            // migration table
            $fileContents = file_get_contents($migrationPath);
            $fileContents = str_replace(
                'use GreenPlate\Crud\CrudCreator\MigrationExtractor\BlueprintExtractor;',
                'use Illuminate\Database\Schema\Blueprint;',
                $fileContents
            );
            $fileContents = str_replace(
                'use GreenPlate\Crud\CrudCreator\MigrationExtractor\SchemaExtractor;',
                'use Illuminate\Database\Migrations\Migration;',
                $fileContents
            );
            $fileContents = str_replace(
                'class ' . $migrationClass,
                'class ' . $migrationClass . ' extends Migration',
                $fileContents
            );
            $fileContents = str_replace(
                'function up(&$migrationInfo)',
                'function up()',
                $fileContents
            );
            $fileContents = str_replace(
                'SchemaExtractor::create($migrationInfo, ',
                'Schema::create(',
                $fileContents
            );
            $fileContents = str_replace(
                'BlueprintExtractor ',
                'Blueprint ',
                $fileContents
            );

            $typesToReplace = [];
            foreach ($modelInfos as $modelInfo) {
                if ($migrationPath == $modelInfo->getMigrationPath()) {
                    $typesToReplace = array_merge($typesToReplace, $modelInfo->getFillables());
                }
            }
            $typesToReplace = array_unique(array_column($typesToReplace, 'type'));

            foreach ($typesToReplace as $type) {
                $migrationFileReplacerPath = CrudHelpers::getColumnTypeMigrationFileReplacerPath($type);

                if ($this->files->exists($migrationFileReplacerPath)) {
                    require_once($migrationFileReplacerPath);

                    $className = CrudHelpers::getColumnTypeMigrationFileReplacerClass($type);
                    $migrationFileReplacer = new $className();

                    $fileContents = $migrationFileReplacer->replace($fileContents);
                }
            }

            file_put_contents(
                $migrationDirectory . '/' . $this->originalFileName . '.php',
                $fileContents
            );
        }
    }
}