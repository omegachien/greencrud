<?php

namespace GreenPlate\Crud\CrudMigrationCreator;

use Illuminate\Database\Migrations\MigrationCreator;

class CrudMigrationCreator extends MigrationCreator
{
    /**
     * Populate the place-holders in the migration stub.
     *
     * @param  string  $name
     * @param  string  $stub
     * @param  string  $table
     * @return string
     */
    protected function populateStub($name, $stub, $table)
    {
        $stub = parent::populateStub($name, $stub, $table);

        $stub = str_replace(
            'use Illuminate\Database\Schema\Blueprint;',
            'use GreenPlate\Crud\CrudCreator\MigrationExtractor\BlueprintExtractor;',
            $stub
        );
        $stub = str_replace(
            'use Illuminate\Database\Migrations\Migration;',
            'use GreenPlate\Crud\CrudCreator\MigrationExtractor\SchemaExtractor;',
            $stub
        );
        $stub = str_replace(
            'extends Migration',
            '',
            $stub
        );
        $stub = str_replace(
            'function up()',
            'function up(&$migrationInfo)',
            $stub
        );
        $stub = str_replace(
            'Schema::create(',
            'SchemaExtractor::create($migrationInfo, ',
            $stub
        );
        $stub = str_replace(
            'Blueprint ',
            'BlueprintExtractor ',
            $stub
        );

        // '--create' option is used
        if (!is_null($table)) {
            $stub = str_replace(
                '$table->timestamps();',
                '$table->timestampsTz();',
                $stub
            );
        }

        return $stub;
    }
}
