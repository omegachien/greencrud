<?php // 2016_09_08_143050_create_test_table.php

use Illuminate\Support\Facades\Schema;
use GreenPlate\Crud\CrudCreator\MigrationExtractor\BlueprintExtractor;
use GreenPlate\Crud\CrudCreator\MigrationExtractor\SchemaExtractor;

class CreateTestTable 
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(&$migrationInfo)
    {
        SchemaExtractor::create($migrationInfo, 'industry', function ($table) {
            $table->increments('id');
            $table->decimal('base_rate', 5, 2)->default(0.00);
            $table->integer('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('industry');

    }
}
