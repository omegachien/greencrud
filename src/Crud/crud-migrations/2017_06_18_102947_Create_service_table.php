<?php // 2017_06_18_102947_Create_service_table.php

use Illuminate\Support\Facades\Schema;
use GreenPlate\Crud\CrudCreator\MigrationExtractor\BlueprintExtractor;
use GreenPlate\Crud\CrudCreator\MigrationExtractor\SchemaExtractor;

class CreateServiceTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(&$migrationInfo)
    {
        SchemaExtractor::create($migrationInfo, 'services', function ($table) {
            $table->increments('id');
            $table->string('name');
            $table->string('test');
//            $table->date('test_date');
//            $table->date('test_date2');
            $table->media('logo', ['multiple' => false]);
            $table->media('image', ['multiple' => true]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('services');
    }
}
