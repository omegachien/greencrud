<?php // 2017_06_27_061639_create_new_something_table.php

use Illuminate\Support\Facades\Schema;
use GreenPlate\Crud\CrudCreator\MigrationExtractor\BlueprintExtractor;
use GreenPlate\Crud\CrudCreator\MigrationExtractor\SchemaExtractor;

class CreateNewSomethingTable 
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(&$migrationInfo)
    {
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
