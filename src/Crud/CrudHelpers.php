<?php

namespace GreenPlate\Crud;

use GreenPlate\Helper\Helper;
use Illuminate\Filesystem\Filesystem;

class CrudHelpers
{
    private static $supportedColumnTypes = [];
    private static $supportedColumnModifiers = [];

    // --------------------------------------------------------------------
    /* Column Types */

    public static function getSupportedColumnTypes()
    {
        static::updateSupportedColumnTypes();

        return array_column(self::$supportedColumnTypes, 'name');
    }

    public static function getColumnTypeMigrationInfoPath($columnType)
    {
        return static::getColumnTypePath($columnType) . '/MigrationInfo.php';
    }

    public static function getColumnTypeMigrationInfoClass($columnType)
    {
        return static::getColumnTypeNamespace($columnType) . '\\MigrationInfo';
    }

    public static function getColumnTypeMigrationFileReplacerPath($columnType)
    {
        return static::getColumnTypePath($columnType) . '/MigrationFileReplacer.php';
    }

    public static function getColumnTypeMigrationFileReplacerClass($columnType)
    {
        return static::getColumnTypeNamespace($columnType) . '\\MigrationFileReplacer';
    }

    /**
     * initializeColumnModelGenerator
     *
     * @param array $modelInfos
     */
    public static function initializeColumnModelGenerator($modelInfos)
    {
        return static::initializeColumnGenerator($modelInfos, 'model');
    }

    /**
     * initializeColumnControllerGenerator
     *
     * @param array $modelInfos
     */
    public static function initializeColumnControllerGenerator($modelInfos)
    {
        return static::initializeColumnGenerator($modelInfos, 'controller');
    }

    /**
     * initializeColumnViewGenerator
     *
     * @param array $modelInfos
     */
    public static function initializeColumnViewGenerator($modelInfos)
    {
        return static::initializeColumnGenerator($modelInfos, 'form');
    }

    /**
     * initializeColumnViewGenerator
     *
     * @param array $modelInfos
     */
    public static function initializeColumnRouteGenerator($modelInfos)
    {
        return static::initializeColumnGenerator($modelInfos, 'route');
    }

    public static function updateSupportedColumnTypes()
    {
        if (empty(self::$supportedColumnTypes)) {
            $files = new Filesystem();

            foreach ($files->directories(__DIR__ . CRUD_COLUMN_TYPE_PATH) as $directoryPath) {
                // Record the column type and directory full path
                self::$supportedColumnTypes[] = [
                    'name' => camel_case(pathinfo($directoryPath, PATHINFO_FILENAME)),
                    'directoryPath' => $directoryPath,
                ];
            }
        }
    }

    // -----

    private static function getColumnTypeClassPath($columnType)
    {
        return __DIR__ . CRUD_COLUMN_TYPE_CLASS_PATH . '/' . ucfirst($columnType);
    }

    private static function getColumnTypePath($columnType)
    {
        return __DIR__ . CRUD_COLUMN_TYPE_PATH . '/' . ucfirst($columnType);
    }

    private static function getColumnTypeNamespace($columnType)
    {
        return ltrim(Helper::pathToPsr4Namespace(CRUD_COLUMN_TYPE_CLASS_PATH . '/' . $columnType), '\\');
    }

    private static function initializeColumnGenerator($modelInfos, $generatorName)
    {
        // get a list of unique fillable type from all tables
        $fillableTypes = [];
        foreach ($modelInfos as $modelInfo) {
            // Union all $fillableTypes
            $fillableTypes =
                array_merge($fillableTypes, $modelInfo->getFillableTypes());
        }
        $fillableTypes = array_unique($fillableTypes);

        $columnGenerators = [];
        foreach ($fillableTypes as $type) {
            $generatorPath = static::getColumnTypePath($type);
            $generatorClass = static::getColumnTypeNamespace($type);

            $ucfirstGeneratorName = ucfirst($generatorName);
            switch ($generatorName) {
                case 'model':
                case 'controller':
                case 'form':
                case 'route':
                    $generatorPath .=
                        "/Column{$ucfirstGeneratorName}Generator/Column{$ucfirstGeneratorName}Generator.php";
                    $generatorClass .=
                        "\\Column{$ucfirstGeneratorName}Generator\\Column{$ucfirstGeneratorName}Generator";
                    break;

                default:
                    return [];
            }

            require_once($generatorPath);
            $columnGenerators[$type] = new $generatorClass;
        }

        return $columnGenerators;
    }

    // --------------------------------------------------------------------
    /* Column Modifiers */

    public static function getSupportedColumnModifiers()
    {
        static::updateSupportedColumnModifiers();

        return array_column(self::$supportedColumnModifiers, 'name');
    }

    public static function getColumnModifierMigrationInfoPath($columnModifier)
    {
        return __DIR__ . CRUD_COLUMN_MODIFIER_PATH . '/' . ucfirst($columnModifier) . '/MigrationInfo.php';
    }

    public static function getColumnModifierMigrationInfoClass($columnModifier)
    {
        return ltrim(Helper::pathToPsr4Namespace(CRUD_COLUMN_MODIFIER_CLASS_PATH . '/' . $columnModifier) . '\\MigrationInfo', '\\');
    }

    public static function updateSupportedColumnModifiers()
    {
        if (empty(self::$supportedColumnModifiers)) {
            $files = new Filesystem();

            foreach ($files->directories(__DIR__ . CRUD_COLUMN_MODIFIER_PATH) as $directoryPath) {
                // Record the column modifier and directory full path
                self::$supportedColumnModifiers[] = [
                    'name' => camel_case(pathinfo($directoryPath, PATHINFO_FILENAME)),
                    'directoryPath' => $directoryPath,
                ];
            }
        }
    }
}
