<?php

define('TAB_SPACE', '    ');

define('CRUD_MIGRATION_PATH', 'GreenPlate/src/Crud/crud-migrations');
define('CRUD_COLUMN_TYPE_PATH', '/CrudCreator/ColumnTypes');
define('CRUD_COLUMN_TYPE_CLASS_PATH', 'GreenPlate/Crud/CrudCreator/ColumnTypes');
define('CRUD_COLUMN_MODIFIER_PATH', '/CrudCreator/ColumnModifiers');
define('CRUD_COLUMN_MODIFIER_CLASS_PATH', 'GreenPlate/Crud/CrudCreator/ColumnModifiers');
