<?php

namespace GreenPlate\Console;

use GreenPlate\Crud\CrudMigrationCreator\CrudMigrationCreator;
use GreenPlate\Crud\CrudBaseCommand;

class NewCrudMigrationCommand extends CrudBaseCommand
{
    /**
     * The console command signature.
     *
     * @var string
     */
    protected $signature = 'codegen:new
        {name : The name of the migration.}
        {--create= : The table to be created.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new crud migration file';

    /**
     * The migration creator instance.
     *
     * @var \GreenPlate\Crud\CrudMigrationCreator\CrudMigrationCreator
     */
    protected $creator;

    /**
     * Create a new migration install command instance.
     *
     * @param \GreenPlate\Crud\CrudMigrationCreator\CrudMigrationCreator $creator
     */
    public function __construct(CrudMigrationCreator $creator)
    {
        parent::__construct();

        $this->creator = $creator;
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        // It's possible for the developer to specify the tables to modify in this
        // schema operation. The developer may also specify if this table needs
        // to be freshly created so we can create the appropriate migrations.
        $name = $this->input->getArgument('name');

        $table = null;

        $create = $this->input->getOption('create');

        if (!$table && is_string($create)) {
            $table = $create;
        }

        // Now we are ready to write the migration out to disk. Once we've written
        // the migration out
        $this->writeMigration($name, $table, $create);
    }

    /**
     * Write the migration file to disk.
     *
     * @param  string  $name
     * @param  string  $table
     * @param  bool    $create
     * @return string
     */
    protected function writeMigration($name, $table, $create)
    {
        $path = base_path(CRUD_MIGRATION_PATH);
        if (!file_exists($path)) {
            mkdir($path, 0775, true);
        }

        $file = pathinfo($this->creator->create($name, $path, $table, $create), PATHINFO_FILENAME);

        $fullFilePath = $path . '/' . $file . '.php';
        $fileContent = file_get_contents($fullFilePath);
        $fileContent = str_replace(
            '<?php',
            '<?php // ' . $file  . '.php',
            $fileContent
        );
        file_put_contents($fullFilePath, $fileContent);

        $this->line("<info>Created Migration:</info> $file");
    }
}
