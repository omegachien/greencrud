<?php

namespace GreenPlate\Console;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;

class BootstrapCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'codegen:bootstrap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create skeleton admin theme';


    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $confirmationMessage = 'The following files will be replaced:' .
            PHP_EOL . 'resources/views/_layout/base.blade.php' .
            PHP_EOL . 'resources/view/_layout/footer.blade.php' .
            PHP_EOL . 'resources/view/_layout/navbar.blade.php' .
            PHP_EOL . 'resources/view/_layout/sidebar-left.blade.php' .
            PHP_EOL . 'resources/view/_layout/sidebar-right.blade.php' .
            PHP_EOL . 'resources/view/home.blade.php' .
            PHP_EOL . 'resources/assets/sass/app.scss' .
            PHP_EOL . 'resources/assets/css/style.css' .
            PHP_EOL . 'resources/lang/en/general.php' .
            PHP_EOL . 'resources/views/vendor/flash/message.blade.php' .
            PHP_EOL . 'config/app.php' .
            PHP_EOL . 'config/filesystems.php' .
            PHP_EOL . 'app/Providers/RouteServiceProvider.php' .
            PHP_EOL . 'webpack.mix.js' .
            PHP_EOL . PHP_EOL . 'The following packages will be installed via npm: ' .
            PHP_EOL . 'admin-lte font-awesome ionicons datatables.net datatablets.net-bs bootbox sortablejs noty' .
            PHP_EOL . PHP_EOL . 'The following packages will be installed via composer: ' .
            PHP_EOL . 'laracasts/flash spatie/laravel-medialibrary barryvdh/laravel-ide-helper';

        $confirmation = $this->confirm($confirmationMessage);
        if ( ! $confirmation) {
            return;
        }

        $this->executeComposer();
        $this->copyFiles();
        $this->executeNpm();
    }

    /**
     * Copy stub files over to relevant folders
     */
    private function copyFiles()
    {
        $viewPath = resource_path('views/_layout');
        if ( ! file_exists($viewPath)) {
            mkdir($viewPath);
        }

        if ( ! file_exists(resource_path('assets/css'))) {
            mkdir(resource_path('assets/css'));
        }

        if ( ! file_exists(resource_path('views/vendor/flash'))) {
            mkdir(resource_path('views/vendor/flash'), 0755, true);
        }

        copy(dirname(__DIR__) . '/Stub/Views/_layout/base.blade.php', $viewPath . '/base.blade.php');
        copy(dirname(__DIR__) . '/Stub/Views/_layout/footer.blade.php', $viewPath . '/footer.blade.php');
        copy(dirname(__DIR__) . '/Stub/Views/_layout/navbar.blade.php', $viewPath . '/navbar.blade.php');
        copy(dirname(__DIR__) . '/Stub/Views/_layout/sidebar-left.blade.php', $viewPath . '/sidebar-left.blade.php');
        copy(dirname(__DIR__) . '/Stub/Views/_layout/sidebar-right.blade.php', $viewPath . '/sidebar-right.blade.php');
        copy(dirname(__DIR__) . '/Stub/Views/home.blade.php', resource_path('views/home.blade.php'));
        copy(dirname(__DIR__) . '/Stub/app.scss', resource_path('assets/sass/app.scss'));
        copy(dirname(__DIR__) . '/Stub/bootstrap.js', resource_path('assets/js/bootstrap.js'));
        copy(dirname(__DIR__) . '/Stub/webpack.mix.js', 'webpack.mix.js');
        copy(dirname(__DIR__) . '/Stub/js/hayageek.uploadfile.min.js', resource_path('assets/js/hayageek.uploadfile.min.js'));
        copy(dirname(__DIR__) . '/Stub/css/hayageek.uploadfile.css', resource_path('assets/css/hayageek.uploadfile.css'));
        copy(dirname(__DIR__) . '/Stub/css/style.css', resource_path('assets/css/style.css'));
        copy(dirname(__DIR__) . '/Stub/php/app.php', 'config/app.php');
        copy(dirname(__DIR__) . '/Stub/php/general.php', resource_path('lang/en/general.php'));
        copy(dirname(__DIR__) . '/Stub/php/filesystems.php', 'config/filesystems.php');
        copy(dirname(__DIR__) . '/Stub/php/RouteServiceProvider.php', 'app/Providers/RouteServiceProvider.php');
        copy(dirname(__DIR__) . '/Stub/Views/vendor/flash/message.blade.php', resource_path('views/vendor/flash/message.blade.php'));
        copy(dirname(__DIR__) . '/Stub/php/backend-routes.php', 'routes/backend-routes.php');
        copy(dirname(__DIR__) . '/Stub/php/web.php', 'routes/web.php');
    }

    private function executeComposer()
    {
        $this->runCommand('composer require laracasts/flash');
        $this->runCommand('php artisan vendor:publish --provider="Laracasts\Flash\FlashServiceProvider"');
        $this->runCommand('composer require spatie/laravel-medialibrary:^6.0.0');
        $this->runCommand('composer require barryvdh/laravel-ide-helper');
        $this->runCommand('php artisan vendor:publish --provider="Spatie\MediaLibrary\MediaLibraryServiceProvider" --tag="migrations"');
        $this->runCommand('php artisan vendor:publish --provider="Spatie\MediaLibrary\MediaLibraryServiceProvider" --tag="config"');
        $this->runCommand('php artisan migrate');
    }

    private function executeNpm()
    {
        $this->runCommand('npm install');
        $this->runCommand('npm install admin-lte --save');
        $this->runCommand('npm install font-awesome --save');
        $this->runCommand('npm install ionicons --save');
        $this->runCommand('npm install datatables.net --save');
        $this->runCommand('npm install datatables.net-bs --save');
        $this->runCommand('npm install bootbox --save');
        $this->runCommand('npm install eonasdan-bootstrap-datetimepicker --save');
        $this->runCommand('npm install sortablejs --save');
        $this->runCommand('npm install noty --save');
        $this->runCommand('npm run dev');
    }

    private function runCommand($command, $message = '')
    {
        if (empty($message)) {
            $message = 'Executing command ' . '\'' . $command . '\'';
        }

        $this->info($message);
        $process = new Process($command);
        $process->setTimeout(3600);
        $process->run(function ($type, $buffer) {
            if (Process::ERR === $type) {
                echo '> ' . $buffer;
            } else {
                echo $buffer;
            }
        });
    }

}