<?php

namespace GreenPlate\Console;

use GreenPlate\Crud\CrudCreator\CrudCreator;
use GreenPlate\Crud\CrudBaseCommand;
use GreenPlate\Helper\Helper;
use Illuminate\Filesystem\Filesystem;

class CrudCommand extends CrudBaseCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'codegen:crud
                            {--migration= : The file name of the crud migration}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates CRUD for the selected crud migration';

    /**
     * The crud creator instance.
     *
     * @var \GreenPlate\Crud\CrudCreator\CrudCreator
     */
    protected $creator;

    /**
     * The filesystem instance.
     *
     * @var \Illuminate\Filesystem\Filesystem
     */
    protected $files;

    /**
     * Create a new controller creator command instance.
     *
     * @param  \GreenPlate\Crud\CrudCreator\CrudCreator $creator
     * @param  \Illuminate\Filesystem\Filesystem $files
     */
    public function __construct(CrudCreator $creator, Filesystem $files)
    {
        parent::__construct();

        $this->creator = $creator;
        $this->files = $files;
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $selectedMigrationFile = $this->input->getOption('migration');

        $crudMigrationList = $this->getCrudMigrationList($selectedMigrationFile);

        // Skip asking for confirmation if '--migrate' is specified
        if (is_null($selectedMigrationFile) &&
                !$this->confirmToProceed($crudMigrationList)) {
            return;
        }

        $this->creator->create($crudMigrationList);

        $this->info('CRUD generated. :)');
    }

    /**
     *  Generate the list of crud migrations that will be run.
     *  If '--migration' is not set, a list from crud-migrations that are not
     *  run yet will be populated. (crud-migrations files that are not found
     *  in the migrations folder are considered not run yet)
     *
     * @param  string $selectedMigrationFile
     * @return array
     */
    protected function getCrudMigrationList($selectedMigrationFile = null)
    {
        // Initialize constants
        $crudMigrationDirectory = base_path(CRUD_MIGRATION_PATH);
        $migrationDirectory = base_path('database/migrations');

        if (!is_null($selectedMigrationFile)) {
            // rtrim and concat '.php', make sure the input from user will work
            // regardless of '.php' is inserted
            $selectedMigrationFile =
                $crudMigrationDirectory . '/' . rtrim($selectedMigrationFile, '.php') . '.php';

            if (!file_exists($selectedMigrationFile)) {
                $this->error($selectedMigrationFile . ' not found!');
                exit(1);
            }

            $crudMigrationFileList = [
                0 => $selectedMigrationFile
            ];
        } else {
            // if no migration file is selected, then just run all the migrations in the
            // crudMigrations directory, but excluding those that already exists in
            // the migrations folder

            $crudMigrationFileList = $this->files->files($crudMigrationDirectory);
            $migrationFileList = $this->files->files($migrationDirectory);

            // extract file name only for comparison later as the full file path
            // to migrations and crud migrations are different
            $migrationFileNameList = [];
            foreach ($migrationFileList as $fullFilePath) {
                $migrationFileNameList[] = pathinfo($fullFilePath, PATHINFO_FILENAME);
            }

            // remove crud migration files that already exist in the migration folder
            foreach ($crudMigrationFileList as $index => $crudFullFilePath) {
                if (in_array(pathinfo($crudFullFilePath, PATHINFO_FILENAME), $migrationFileNameList)) {
                    unset($crudMigrationFileList[$index]);
                }
            }
        }
        sort($crudMigrationFileList); // we dont want to preserve index here

        // Format $crudMigrationList into,
        //
        // array:1 [
        //      "Test" => "/<full-path>/crud-migrations/2016_08_15_182143_create_test_table.php"
        // ]
        //
        $crudMigrationList = [];
        foreach ($crudMigrationFileList as $crudFullFilePath) {
            $crudMigrationList[Helper::fileGetPhpClass($crudFullFilePath)] = $crudFullFilePath;
        }

        return $crudMigrationList;
    }

    /**
     * Output the list of crud migration that will be run and ask for
     * confirmation
     *
     * @param  array $crudMigrationList
     * @return boolean
     */
    protected function confirmToProceed($crudMigrationList)
    {
        if (empty($crudMigrationList)) {
            $this->comment('No new crud migration found for CRUD generation.');
            $this->comment('Command Aborted!');

            return false;
        }

        $this->comment('Crud will be generated for these migrations :');
        foreach ($crudMigrationList as $crudMigrationFullPath) {
            $this->comment('"' . $crudMigrationFullPath . '"');
        }

        $confirmed = $this->confirm('Do you really wish to run this command?');
        if (! $confirmed) {
            $this->comment('Command Cancelled!');

            return false;
        }

        return true;
    }
}
