<?php
namespace GreenPlate;

use GreenPlate\Console\BootstrapCommand;
use GreenPlate\Console\CrudCommand;
use GreenPlate\Console\NewCrudMigrationCommand;
use Illuminate\Support\ServiceProvider;

class GreenPlateServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                CrudCommand::class,
                NewCrudMigrationCommand::class,
                BootstrapCommand::class,
            ]);
        }

//        $this->loadRoutesFrom(dirname(__DIR__).'/Route/routes.php');
//        $this->app->make('GreenCrud\Controllers\GreenCrudController');
//        $this->loadViewsFrom(dirname(__DIR__).'/Views/', 'greencrud');
    }

    public function register()
    {

    }
}