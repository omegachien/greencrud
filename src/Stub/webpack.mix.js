const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css/app.css');

mix.styles([
    'node_modules/admin-lte/dist/css/AdminLTE.css',
    'node_modules/admin-lte/dist/css/skins/skin-blue.css',
    'node_modules/datatables.net-bs/css/dataTables.bootstrap.css',
    'resources/assets/css/hayageek.uploadfile.css',
    'resources/assets/css/style.css'
], 'public/css/admin_lte_compiled.css');

mix.scripts([
    'node_modules/admin-lte/plugins/slimScroll/jquery.slimscroll.js',
    'node_modules/sortablejs/Sortable.js',
    'node_modules/admin-lte/plugins/fastclick/fastclick.js',
    'node_modules/admin-lte/dist/js/adminlte.js',
    'resources/assets/js/hayageek.uploadfile.min.js',
    'node_modules/datatables.net/js/jquery.dataTables.js',
    'node_modules/datatables.net-bs/js/dataTables.bootstrap.js',
    'node_modules/bootbox/bootbox.js'
], 'public/js/admin_lte_compiled.js');