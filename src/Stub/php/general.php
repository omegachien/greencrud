<?php

return [
    'create_success' => ':name created successfully.',
    'create_fail'    => 'Error in creating :name.',
    'update_success' => ':name updated successfully.',
    'update_fail'    => 'Error in updating :name.',
    'delete_success' => ':name deleted successfully.',
    'delete_fail'    => 'Error in deleting :name.',
];