<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Starter</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="{{ mix('css/app.css') }}">
  <link rel="stylesheet" href="{{ mix('css/admin_lte_compiled.css') }}">

@section('customCss')

@show
<!-- https://adminlte.io/themes/AdminLTE/documentation/index.html -->

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition skin-blue fixed sidebar-mini">
<div class="wrapper">
  @include('_layout.navbar')
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <div class="user-panel">
        <div class="pull-left image">
          <img src="https://adminlte.io/themes/AdminLTE/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>John Doe</p>
        </div>
      </div>
      <ul class="sidebar-menu" data-widget="tree">
        @include('_layout.sidebar-left')
      </ul>
    </section>
  </aside>

  <div class="content-wrapper">
    <section class="content container-fluid">

    @include('flash::message')
    @yield('mainContent')
    </section>
  </div>

  @include('_layout.footer')
  @include('_layout.sidebar-right')
</div>

<script>
    var AdminLTEOptions = {
        sidebarExpandOnHover: true,
    };
</script>
<script src="{{mix('js/app.js')}}"></script>
<script src="{{mix('js/admin_lte_compiled.js')}}"></script>
<script>
    $(document).ready(function () {
        $(".sidebar-menu").slimscroll({
            height: ($(window).height() - $(".main-header").height() - ($('.sidebar-form').outerHeight(true) || 0)) - ($('.user-panel').outerHeight(true) || 0) + "px",
            color: "rgba(0,0,0,0.2)",
            size: "3px"
        });

    });
</script>
@section('customJs')

@show

</body>
</html>