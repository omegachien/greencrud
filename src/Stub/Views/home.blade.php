@extends('_layout.base')

@section('mainContent')
  <div class="row">
    <div class="col-xs-12">
      <div class="panel panel-default backend-view">
        <div class="panel-heading">
          READ ME
        </div>
        <div class="panel-body" style="min-height: 200px;">
          <div>
            Theme is based on AdminLTE, you can have a look at the <a target="_blank"
                                                                      href="https://adminlte.io/themes/AdminLTE/index.html">documentation</a>
            to see all the elements that's included.
          </div>
          <div>
            <p>
              PHP packages included:
            </p>
            <ul>
              <li><a target="_blank" href="https://github.com/spatie/laravel-medialibrary">spatie/laravel-medialibrary</a></li>
              <li><a target="_blank" href="https://github.com/laracasts/flash">laracasts/flash</a></li>
              <li><a target="_blank" href="https://github.com/barryvdh/laravel-ide-helper"> barryvdh/laravel-ide-helper</a></li>
            </ul>
            <div>
              <p>
                NPM packages included:
              </p>
              <ul>
                <li>admin-lte</li>
                <li>bootbox</li>
                <li>datatables.net</li>
                <li>datatables.net-bs</li>
                <li>font-awesome</li>
                <li>ionicons</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection