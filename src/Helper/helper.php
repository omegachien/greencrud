<?php

namespace GreenPlate\Helper;

class Helper
{
    public static function fileGetPhpClass($filePath)
    {
        $class = '';

        $phpCode = file_get_contents($filePath);
        $tokens = token_get_all($phpCode);
        $count = count($tokens);
        for ($i = 2; $i < $count; $i ++) {
            if ($tokens[$i - 2][0] == T_CLASS &&
                $tokens[$i - 1][0] == T_WHITESPACE &&
                $tokens[$i][0] == T_STRING
            ) {
                $class = $tokens[$i][1];
                break;
            }
        }

        return $class;
    }

    public static function pathToPsr4Namespace($classPath)
    {
        // Replace ' ', '-' and '/' into '\'
        $replaceList = [' ', '-', '/'];
        $namespace = str_replace($replaceList, '\\', $classPath);

        // Capitalize every word after \
        $namespace = implode('\\', array_map('ucfirst', explode('\\', $namespace)));

        return $namespace;
    }

    public static function stringLinePrefix($prefix, $string)
    {
        if (!empty($string)) {
            $stringArray = explode("\n", static::EolToUnix($string));

            foreach ($stringArray as $index => $singleLine) {
                $stringArray[$index] = empty($singleLine) ? $singleLine : $prefix . $singleLine;
            }

            $string = implode("\n", $stringArray);
        }

        return $string;
    }

    public static function EolToUnix($string)
    {
        // make sure both "\r\n" and "\r" are taken care of
        $string = str_replace("\r\n", "\n", $string);
        $string = str_replace("\r", "\n", $string);

        return $string;
    }

    public static function createIfNotExists($path)
    {
        if (!file_exists($path)) {
            mkdir($path, 0775, true);
        }
    }

    public static function spinalCase($string)
    {
        return str_replace('_', '-', snake_case($string));
    }
}